# /packages/intranet-trans-trados/trados/update-tmbs.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Update the TM & TB from the company, to bring the project up to date

    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2011-08-05
} {
    {project_id ""}
}

# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [ad_maybe_redirect_for_registration]

im_trans_trados_project_info -project_id $project_id -array_name project_info

set project_dir $project_info(project_dir)
set trados_dir $project_info(trados_dir)

    
set perm_p 0
# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
im_project_permissions $user_id $project_id view read write admin
if {$admin} { set perm_p 1 }
    
# Users with "add_projects" privilege can always create new projects...
if {[im_permission $user_id add_projects]} { set perm_p 1 } 
if {!$perm_p} { 
    ad_return_complaint "Insufficient Privileges" "
        <li>You don't have sufficient privileges to see this page."
    return
}


set project_query "
    select
        p.project_nr,
        p.source_language_id,
        company_id,
        final_company_id,
        subject_area_id
    from
        im_projects p
    where
        p.project_id=:project_id
"

if { ![db_0or1row projects_info_query $project_query] } {
    ad_return_complaint 1 "[_ intranet-translation.lt_Cant_find_the_project]"
    return
}

# Create the language mappings
set target_language_ids [im_target_language_ids $project_id]

foreach target_language_id $target_language_ids {

    # Find the correct translation memories
    set tm_paths [im_trans_trados_get_trans_memory_file \
        -company_id $company_id \
        -final_company_id $final_company_id \
        -source_language_id $source_language_id \
        -target_language_id $target_language_id \
        -subject_area_id $subject_area_id]


    foreach file_path $tm_paths {
        set filename [lindex [split $file_path "/"] end]
        file copy -force $file_path "${trados_dir}/TM"            
    }
}

ad_returnredirect [export_vars -base "/intranet/projects/view" -url {project_id}]