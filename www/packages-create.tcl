ad_page_contract {
    @author malte.sussdorff@cognovis.de
} {
    project_id:integer
}

db_1row project_info "select to_char(start_date,'YYYY-MM-DD HH24:MI') as project_start_date, project_name, project_status_id,company_id, subject_area_id,source_language_id from im_projects where project_id = :project_id"

set target_language_ids [list]
set task_type_ids [list]

db_foreach task_names {
	select aux_string1, target_language_id
	from im_trans_tasks, im_categories
	where project_id = :project_id
	and category_id = task_type_id
} {
	if {[lsearch $target_language_ids $target_language_id]<0} {lappend target_language_ids $target_language_id}

	# Add the task type ids
	foreach task_type $aux_string1 {
		set task_type_id [db_string type_id "select category_id from im_categories where lower(category) = :task_type and category_type = 'Intranet Trans Task Type'" -default ""]
		if {$task_type_id ne ""} {
			if {[lsearch $task_type_ids $task_type_id]<0} {lappend task_type_ids $task_type_id}
		}
	}
}

set project_dir [cog::project::path -project_id $project_id]

foreach target_language_id $target_language_ids {
    foreach task_type_id $task_type_ids {

	# ---------------------------------------------------------------
	# Auto Create Packages if we have trados packages
	# But only if no package was created already
	# ---------------------------------------------------------------
	set freelance_package_id [db_string exists "select fp.freelance_package_id
			from im_freelance_packages fp, im_freelance_packages_trans_tasks fptt, im_trans_tasks tt
			where fp.freelance_package_id = fptt.freelance_package_id
				and fptt.trans_task_id = tt.task_id
				and fp.package_type_id = :task_type_id
				and fp.project_id = :project_id
				and tt.target_language_id = :target_language_id limit 1" -default 0]

	set create_package_p 1
	set upload_file_p 1

	if {$freelance_package_id ne ""} {
	    set create_package_p 0
	}

        set package_file_type_id 606
        set task_type [im_category_from_id -translate_p 0 $task_type_id]

      	# Check if we have the package
       	foreach package_file [im_trans_trados_packages -project_id $project_id -target_language_id $target_language_id -task_type $task_type] {
	    
	    set file_path [lindex $package_file 0]
	    set file_task_ids [lindex $package_file 1]
	    set freelance_package_name "${project_name}_[im_category_from_id -translate_p 0 $target_language_id]_[im_category_from_id -translate_p 0 $task_type_id]"
	    
	    if {$create_package_p} {
		# We don't have a package yet, so create one for the tasks found
		set freelance_package_id [im_freelance_package_create -trans_task_ids $file_task_ids \
					      -package_type_id $task_type_id \
					      -freelance_package_name $freelance_package_name]
	    }
	    
	    # Check if we have a file (Trados out) associated with it.
	    set freelance_package_file_id [db_string file_exists "select freelance_package_file_id from im_freelance_package_files
where freelance_package_id = :freelance_package_id and package_file_type_id = 606" -default ""]
	    
	    if {$freelance_package_file_id ne ""} {
		set upload_file_p 0
	    } 

	    if {$upload_file_p} {
		set folder [im_trans_task_folder -project_id $project_id -target_language [im_category_from_id -translate_p 0 $target_language_id] -folder_type $task_type]
		set extension [file extension $file_path]
		set freelance_package_file_name "${freelance_package_name}$extension"
		set new_path "${project_dir}/${folder}/$freelance_package_file_name"
		file rename -force $file_path $new_path
		
		set freelance_package_file_id [im_freelance_package_file_create \
						   -freelance_package_id $freelance_package_id \
						   -file_path $new_path \
						   -freelance_package_file_name $freelance_package_file_name \
						   -package_file_type_id $package_file_type_id]
	    }
	}
    }
}
ad_returnredirect "[export_vars -base "/intranet/projects/view" -url {project_id}]"


