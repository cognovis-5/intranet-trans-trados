# Copyright (C) 2003 - 2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_page_contract {
    Delete a TM

    @param file_name
    @param company_id
    @param return_url

} {
   
    {file_name:multiple,optional ""}
    company_id:notnull
    return_url:notnull
}
# ---------------------------------------------------------------
# We utterly ignore permissions for the time being.
# If wanted, we need to check how filestorage/delete.tcl handles it
# ---------------------------------------------------------------

foreach tm_file $file_name {
    set company_trados_path [im_trans_trados_folder -company_id $company_id]
    set file_extension [file extension $file_name]
    # Find the right folder depending on filetype
    switch $file_extension {
	.sdltm {
	    set file_path "${company_trados_path}/TM/$tm_file"
	}
	.sdltb {
	    set file_path "${company_trados_path}/TB/$tm_file"
	}
	default {
	    set file_path ""
	}
    }
    if {$file_path ne ""} {
	file delete -force $file_path
    }
}

ad_returnredirect $return_url
