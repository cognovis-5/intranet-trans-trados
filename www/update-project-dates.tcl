# /packages/intranet-filestorage/www/erase-folder.tcl
#
# Copyright (C) 2003 - 2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_page_contract {
    Update the project dates based on the processing time

    @param project_id

} {
    project_id:notnull
}

# ---------------------------------------------------------------
# The start date is now. 
# Therefore we can focus on calculation the end date
# ---------------------------------------------------------------
set start_timestamp [db_string start_date "select to_char(now(),'YYYY-MM-DD HH24:MI') from dual"]

im_translation_update_project_dates -project_id $project_id -start_timestamp $start_timestamp        

ad_returnredirect [export_vars -base "/intranet/projects/view" -url {project_id}]






