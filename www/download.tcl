# /intranet-trans-trados/www/download.tcl

ad_page_contract {
    see if this person is authorized to read the file in question
    guess the MIME type from the original client filename
    have the Oracle driver grab the BLOB and write it to the connection

} {
    file_path:notnull
}

set user_id [ad_maybe_redirect_for_registration]
set guessed_file_type [ns_guesstype $file_path]


if [file readable $file_path] {
    set outputheaders [ns_conn outputheaders]
    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=[lindex [split $file_path "/"] end]"
    ns_returnfile 200 $guessed_file_type $file_path
} else {
    ad_returnredirect "/error.tcl"
}
