# /packages/intranet-trans-trados/www/upload-file.tcl
#
# Copyright (c) 2003-2007 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_page_contract {
    Upload a Trados wordcount (.CSV) file and convert
    every line of it into an im_task for the Translation
    Workflow.
    The main work is done by "trados-import.tcl", so we
    basically only have to provide the trados file.

    @param project_id The parent project
    @param return_url Where to go after the work is done?
    @param wordcount_application Allows to upload data from
           various Translation Memories
    @param tm_type_id Really necessary? Not used yet, 
           because the TM type is given from the wordcount_app
    @param task_type_id determines the task type
    @param upload_file The filename to be uploaded - according
           to AOLServer conventions

    @author frank.bergmann@project-open.com
} {
    project_id:integer
    return_url
    { target_language_id "" }
	{ quote_p 0 }
    upload_file
} 

# ---------------------------------------------------------------------
# Defaults & Security
# ---------------------------------------------------------------------

set user_id [ad_maybe_redirect_for_registration]
im_project_permissions $user_id $project_id view read write admin
if {!$write} {
    ad_return_complaint 1 "<li>[_ intranet-translation.lt_You_have_insufficient_3]"
    return
}

set project_query "
	select
		p.project_nr,
		p.source_language_id
	from
		im_projects p
	where
		p.project_id=:project_id
"

if { ![db_0or1row projects_info_query $project_query] } {
	ad_return_complaint 1 "[_ intranet-translation.lt_Cant_find_the_project]"
	return
}

# ---------------------------------------------------------------
# Put the uploaded source files into the source directory
# ---------------------------------------------------------------
    
set project_dir [cog::project::path -project_id $project_id]

if {[exists_and_not_null upload_file]} {
   	set source_dir "$project_dir/Trados/Originals"
   	set tmp_filename [ns_queryget upload_file.tmpfile]
   	set filename [ns_urldecode $upload_file]

	# !!!!!!!!!!
	# CHANGE TO ONLY COPY IF FILE NOT ALREADY PRESENT IN ORIGINAL FOLDER
	# !!!!!!!!!!!!!!
	
   	file copy -force $tmp_filename "${source_dir}/$filename"
       
    if {[im_trans_trados_configured_p]} {
  	# ---------------------------------------------------------------
	# Do the trados analysis on our own
	# ---------------------------------------------------------------
	
	im_trans_trados_create_analysis -project_id $project_id -filename $filename -target_language_id $target_language_id

   	# Update the processing time
   	set days [im_translation_processing_time -project_id $project_id -include_pm -update]
	
	# Find the latest quote and replace it if wanted
	if {$quote_p} {
   		set invoice_id [db_string quote "select max(cost_id) from im_costs where project_id = :project_id and cost_type_id = [im_cost_type_quote]" -default ""]
   		if {$invoice_id ne ""} {
   			im_trans_invoice_create_from_tasks -project_id $project_id -invoice_id $invoice_id
   		} else {
   			# No quote to replace
   			set invoice_id [im_trans_invoice_create_from_tasks -project_id $project_id]
   		}
   		ad_returnredirect [export_vars -base "/intranet-invoices/view" -url {invoice_id}]
   	} 
    }
}

ad_returnredirect $return_url
