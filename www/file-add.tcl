ad_page_contract {
    page to add a new translation memory
    
    @author malte.sussdorff@cognovis.de
    @creation-date 2015-11-12
    @cvs-id $Id$
} {
    company_id:integer,notnull
	{project_id ""}
    return_url:optional
}

if {[catch {package require sqlite3}]} {
    set sqlite_p 0
} else {
    set sqlite_p 1
}

set user_id [ad_conn user_id]
set from_addr [party::email -party_id $user_id]
set target_language_options [db_list_of_lists languages "select category,category_id from im_categories where category_type = 'Intranet Translation Language' and enabled_p = 't' order by sort_order, category"]
set target_language_options [concat [list [list "" ""]] $target_language_options]
set company_name [im_name_from_id $company_id]
ad_form -html { enctype multipart/form-data } -export { company_id project_id } -form {
    file_id:key
    {upload_file:file {label \#file-storage.Upload_a_file\#} {html "size 30"}}
}   
  
ad_form -extend -form {

    {subject_area_id:integer(im_category_tree),optional
    	{label "Subject Area"}
    	{custom {category_type "Intranet Translation Subject Area" translate_p 1}}
    }
} 

if {$sqlite_p eq 0} {
    # Append the source and target language
    ad_form -extend -form {
	{source_language_id:integer(im_category_tree),optional
	    {label "[_ intranet-translation.Source_Language]"}
	    {custom {category_type "Intranet Translation Language" translate_p 1}}
	}
	{target_language_id:integer(im_category_tree),optional
	    {label "[_ intranet-translation.Target_Language]"}
	    {custom {category_type "Intranet Translation Language" translate_p 1}}
	}
    }
}

if {$project_id ne ""} {
    ad_form -extend -form {
	{update_p:text(checkbox),optional 
    	{label "[lang::message::lookup \"\" intranet-trans-trados.update_tm_p \"Update TM?\"]"}
    	{help_text "[lang::message::lookup \"\" intranet-trans-trados.update_tm_help \"If checked and the file is a TM we will try to update the existing version, otherwise we will replace it\"]"}
    	{options {{"Yes" "1"}}}
    }
    }
}

ad_form -extend -on_request {
	set update_p 1
}

if {[exists_and_not_null return_url]} {
    ad_form -extend -form {
		{return_url:text(hidden) {value $return_url}}
    }
}

if {[exists_and_not_null project_id]} {
	ad_form -extend -form {} -new_request {
		db_1row project_info "		select subject_area_id, source_language_id from im_projects where project_id = :project_id"
	}
}

ad_form -extend -form {} -new_data {
    
	set upload_tmpfile [template::util::file::get_property tmp_filename $upload_file]
	set upload_filename [template::util::file::get_property filename $upload_file]
	set file_extension [file extension $upload_filename]
	
	set mime_type [cr_filename_to_mime_type -create -- $upload_filename]

	if { ![exists_and_not_null update_p] } {
		set update_p "0"
	}
		
	set company_trados_path [im_trans_trados_folder -company_id $company_id]
	if {![file exists "$company_trados_path"]} {
	    file mkdir "$company_trados_path"
	}
	
	switch $file_extension {
		.sdltm {
			# Find out languages in the Translation Memory
		    if {$sqlite_p} {
			sqlite3 db1 $upload_tmpfile
			set languages [db1 eval "select source_language,target_language from translation_memories"]
			set source_language [lindex $languages 0]
			set target_language [lindex $languages 1]
			
			set source_language_id [cog_category_id -category $source_language -category_type "Intranet Translation Language"]
			set target_language_id [cog_category_id -category $target_language -category_type "Intranet Translation Language"]
		    }
		
		    set filename [im_trans_trados_filename \
				      -company_id $company_id \
				      -source_language_id $source_language_id \
				      -target_language_id $target_language_id \
				      -subject_area_id $subject_area_id \
				      -extension $file_extension
				 ]
		    append company_trados_path "/TM"
		    if {![file exists "$company_trados_path"]} {
			file mkdir "$company_trados_path"
		    }
		    
		    if {$update_p} {
			# Find out if the file already exists for the company, aka is it a real update
			set file_exists_p [file exists "${company_trados_path}/$filename"]
			if {$file_exists_p} {
			    # Update the TM
			    if {$project_id ne ""} {
				im_trans_trados_update_tm -current_tm ${company_trados_path}/$filename -new_tm $upload_tmpfile	-project_id $project_id
			    } else {
				ad_return_error "No Update without project" "Can't update the TM outside a project"
			    }
			} else {
			    # Just copy over, as there is no file
			    file copy $upload_tmpfile "${company_trados_path}/$filename"
			    file attributes "${company_trados_path}/$filename" -permissions "ug+w" 
			}							
		    } else {
			# Overwrite if present
			file copy -force $upload_tmpfile "${company_trados_path}/$filename"
			file attributes "${company_trados_path}/$filename" -permissions "ug+w" 
			}
		}
	    .sdltb {
		set filename [im_trans_trados_filename \
				  -company_id $company_id \
				  -extension $file_extension
			     ]
		append company_trados_path "/TB"
		if {![file exists "$company_trados_path"]} {
		    file mkdir "$company_trados_path"
		}

		set file_exists_p [file exists "${company_trados_path}/$filename"]

		# Overwrite if present
		file copy -force $upload_tmpfile "${company_trados_path}/$filename"
		file attributes "${company_trados_path}/$filename" -permissions "ug+w" 
	    }
	}
} -after_submit {
      	if {[exists_and_not_null return_url]} {
	    	ad_returnredirect $return_url
    	} else {	
	    	ad_returnredirect "./?company_id=$company_id"
    	}
    ad_script_abort	
	
}

ad_return_template
