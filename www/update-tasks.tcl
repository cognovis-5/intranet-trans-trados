# /packages/intranet-trans-trados/www/update-tasks.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
	Update the tasks from the trados folder
} {
	{quote_p "0"}
    { delete_not_included_p "0" }
	project_id:integer
    {return_url ""}
} 

# ---------------------------------------------------------------------
# Defaults & Security
# ---------------------------------------------------------------------

set user_id [ad_maybe_redirect_for_registration]
im_project_permissions $user_id $project_id view read write admin
if {!$write} {
    ad_return_complaint 1 "<li>[_ intranet-translation.lt_You_have_insufficient_3]"
    return
}

# ---------------------------------------------------------------
# Analyse the trados XML File and create the tasks
# ---------------------------------------------------------------
im_trans_trados_project_info -project_id $project_id -array_name project_info
	
	
set project_dir $project_info(project_dir)
set trados_dir $project_info(trados_dir)

if {"${project_dir}/Trados" ne $trados_dir} {
	im_filestorage_rsync -delete -source_path $trados_dir -target_path "${project_dir}/Trados"
}
	
# ---------------------------------------------------------------
# Load the results fo the analysis
# ---------------------------------------------------------------
	
set modification_date ""

db_1row project_info "select project_name, source_language_id from im_projects where project_id = :project_id"
set source_language [im_name_from_id $source_language_id]

set created_task_ids [list]

foreach target_language [im_target_languages $project_id] {
		
	# Get the latest version of the wordcount analysis
	set wc_xml_file ""
	set modification_date ""
	
	if {[catch {glob -directory "${project_dir}/Trados/Reports" *${source_language}_${target_language}*} wc_files]} {
		set wc_files [list]
	}

	foreach wc_file $wc_files {
		set mod [file mtime $wc_file]
		if {$mod>$modification_date} {
			set modification_date $mod
			set wc_xml_file $wc_file
	 	}
	}
		
	if {$wc_xml_file ne ""} {
		set target_language_id [cog_category_id -category $target_language -category_type "Intranet Translation Language"]
	    set wc_task_ids [im_trans_trados_create_tasks \
			-project_id $project_id \
			-trados_analysis_xml $wc_xml_file \
				 -target_language_id $target_language_id]
	    set created_task_ids [concat $created_task_ids $wc_task_ids]
		
	} else {
		ns_log Debug "Could not find WC XML File for $target_language in $project_name"
	}
}

if {$delete_not_included_p && $created_task_ids ne ""} {
    
    # Get the list of tasks not included
    set not_included_task_ids [db_list not_updated "select task_id from im_trans_tasks where project_id = :project_id and task_id not in ([ns_dbquotelist $created_task_ids])"]

    foreach task_id $not_included_task_ids {
	# Check if they are in an assignment. If not.... delete
	set assignment_p [db_string assignment_p "select 1 from im_freelance_assignments fa, im_freelance_packages_trans_tasks fptt where fa.freelance_package_id = fptt.freelance_package_id and fptt.trans_task_id = :task_id and assignment_status_id != 4230" -default 0]
	if {!$assignment_p} {
	    db_dml delete_package_task "delete from im_freelance_packages_trans_tasks where trans_task_id = :task_id"
	    db_dml update_items "update im_invoice_items set task_id = null where task_id = :task_id"
	    db_dml delete_trans_task "delete from im_trans_tasks where task_id = :task_id"
	}
    }
}

# Update the processing time
set days [im_translation_processing_time -project_id $project_id -include_pm -update]
    
# Find the latest quote and replace it if wanted
if {$quote_p} {
  	set invoice_id [db_string quote "select max(cost_id) from im_costs where project_id = :project_id and cost_type_id = [im_cost_type_quote]" -default ""]
   	if {$invoice_id ne ""} {
   	    im_trans_invoice_create_from_tasks -project_id $project_id -invoice_id $invoice_id
   	} else {
   	    # No quote to replace
   	    set invoice_id [im_trans_invoice_create_from_tasks -project_id $project_id]
   	}
    if {$invoice_id eq ""} {
	ad_returnredirect $return_url
    } else {
   	ad_returnredirect [export_vars -base "/intranet-invoices/view" -url {invoice_id}]
}
}

# No quote or no upload file, redirect to the trans task page 
ad_returnredirect "/intranet-translation/trans-tasks/task-list?[export_url_vars project_id return_url]"

