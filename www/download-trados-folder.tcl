# /packages/intranet-trans-trados/trados/download-trados-folder.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Download the project / trados folder of the project
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2011-08-05
} {
    {project_id ""}
}


set user_id [ad_maybe_redirect_for_registration]

# ---------------------------------------------------------------------
# Get some more information about the project
# ---------------------------------------------------------------------

set project_query "
    select
        p.project_nr
    from
        im_projects p
    where
        p.project_id=:project_id
"

if { ![db_0or1row projects_info_query $project_query] } {
    ad_return_complaint 1 "[_ intranet-translation.lt_Cant_find_the_project]"
    return
}
   
set perm_p 0
# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
im_project_permissions $user_id $project_id view read write admin
if {$admin} { set perm_p 1 }
    
# Users with "add_projects" privilege can always create new projects...
if {[im_permission $user_id add_projects]} { set perm_p 1 } 
if {!$perm_p} { 
    ad_return_complaint "Insufficient Privileges" "
        <li>You don't have sufficient privileges to see this page."
    return
}

 
set project_dir [cog::project::path -project_id $project_id]

# Zip the PDF folder and return it.
file delete /tmp/${project_nr}.zip
set zipfile /tmp/${project_nr}.zip
intranet_chilkat::create_zip -directories "$project_dir" -zipfile $zipfile

# Return the file
 set outputheaders [ns_conn outputheaders]
 ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"${project_nr}.zip\""
ns_returnfile 200 application/zip $zipfile

exec rm -rf $zipfile
