# /packages/intranet-trans-trados/tcl/intranet-trans-trados-file-procs.tcl


ad_library {
    Bring together all file operations
    related to the Translation sector for TRADOS

	@author malte.sussdorff@cognovis.de
}

ad_proc -public im_trans_trados_folder {
    -company_id
    -recreate:boolean
} {
    Returns the path to the trados folder for the company
} {
    
    db_1row path "select company_type_id, company_path from im_companies where company_id = :company_id"

    # Only customers should have a trados folder
    set customer_or_internal_type_ids [im_sub_categories -include_disabled_p 1 10246]
    lappend customer_or_internal_type_ids 10247
    if {[lsearch $customer_or_internal_type_ids $company_type_id]<0} {
		return ""
    } else {
		# Check if the folder already exists
		set trados_unix_path [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosCompanyUnixPath"]
		if {$trados_unix_path eq ""} {
			set trados_unix_path [parameter::get -package_id [im_package_filestorage_id] -parameter "CompanyBasePathUnix" -default "/tmp/companies"]
		}

		set trados_folder "$trados_unix_path/$company_path"
		if {![file isdirectory $trados_folder]} {
			# Create the folder
			set folder_created_p [im_filestorage_mkdir -dir $trados_folder]
			if {$folder_created_p eq 1} {
				file mkdir $trados_folder/TM
				file mkdir $trados_folder/TB
			}
		} else {
			set folder_created_p 0
		}
		return "$trados_folder"
    }
}

ad_proc -public im_trans_trados_project_folder {
	-project_id
} {
	Returns the path to the trados folder for the project
} {
	return 	"[cog::project::path -project_id $project_id]/Trados"
}

ad_proc im_trans_trados_filename {
	-company_id
	{-source_language_id ""}
	{-target_language_id ""}
	-extension
	{-subject_area_id ""}
} {

	Return the filename for a translation memory
} {
	db_1row company_info "select company_path from im_companies where company_id = :company_id"
	
	switch $extension {
		.sdltm {
			set filename "TM__${company_path}"
			if {$source_language_id eq "" || $target_language_id eq ""} {
				return ""
			} else {
				append filename "__[im_category_from_id -translate_p 0 $source_language_id]"
				append filename "__[im_category_from_id -translate_p 0 $target_language_id]"
			}
			if {$subject_area_id ne ""} {
				append filename "__[im_category_from_id -translate_p 0 $subject_area_id]"
			}
		}
		.sdltb {
			set filename "TB__${company_path}"
		}
		.sdltpl {
			set filename "PT__${company_path}"
			if {$source_language_id ne ""} {
				append filename "__[im_category_from_id -translate_p 0 $source_language_id]"
				if {$target_language_id ne ""} {
					append filename "__[im_category_from_id -translate_p 0 $target_language_id]"
				}
			}
			if {$subject_area_id ne ""} {
				append filename "__[im_category_from_id -translate_p 0 $subject_area_id]"
			}
		}
		default {
			set filename ""
		}
	}

	if {$filename ne ""} {
		append filename $extension
	}
	return $filename
}

ad_proc im_trans_trados_project_info {
	-project_id
	-array_name
} {
	Return the windows and unix path of the working dir in the array.
	
} {
	upvar 1 $array_name project_info
	if {[info exists project_info]} {
		unset project_info
	}
	db_1row project_info "select
		p.project_nr,
		p.project_path,
		p.project_name,
		c.company_path,
		p.company_id,
		p.final_company_id
	from
		im_projects p,
		im_companies c
	where
		p.project_id=:project_id
		and p.company_id=c.company_id"
	
    set project_dir [cog::project::path -project_id $project_id]
    set trados_dir [im_trans_trados_project_folder -project_id $project_id]	

	# Get the windows paths
	set trados_win_path [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosWinPath"]
	set trados_dir_win "$trados_win_path\\$company_path\\$project_nr\\Trados"
	set project_info(trados_dir) $trados_dir
	set project_info(trados_dir_win) $trados_dir_win
	set project_info(project_nr) $project_nr
	set project_info(project_dir) $project_dir
	set project_info(company_id) $company_id
	set project_info(final_company_id) $final_company_id
    
    # Hard coded for the time being
	set project_info(project_tmp_dir) "/var/lib/aolserver/projop/tmp/$project_nr"
    set project_info(project_tmp_dir_win) "C:\\Users\\strado\\TMP\\$project_nr"
	return 1
}


ad_proc im_trans_trados_get_trans_memory_file {
	-company_id
	{-final_company_id ""}
	-source_language_id
	-target_language_id
	{-subject_area_id ""}
} {
	Returns a list of lists of item_id and filename of the translation memory

	Translation Memories are always language dependend.
	Find the TM of the final customer, then the customer.

	Always use the common TM found for the language combination.
} {
	set result [list]

	# Check if we have a final customer's TM
	if {$final_company_id ne ""} {
		set tm_file_path [im_trans_trados_find_tm -company_id $final_company_id \
			-source_language_id $source_language_id -target_language_id $target_language_id \
			-subject_area_id $subject_area_id]

		if {$tm_file_path ne ""} {lappend result $tm_file_path}
	}

	# Check if we have one for the customer
	if {[llength $result] == 0} {
		set tm_file_path [im_trans_trados_find_tm -company_id $company_id \
			-source_language_id $source_language_id -target_language_id $target_language_id \
			-subject_area_id $subject_area_id]

		if {$tm_file_path ne ""} {lappend result $tm_file_path}
	}


	# Always append the default one from internal company
	set tm_file_path [im_trans_trados_find_tm -company_id [im_company_internal] \
-source_language_id $source_language_id -target_language_id $target_language_id \
-subject_area_id $subject_area_id]

	if {$tm_file_path ne ""} {lappend result $tm_file_path}

	return $result
}

ad_proc -public im_trans_trados_find_tm {
	-company_id
	-source_language_id
	-target_language_id
	{-subject_area_id ""}
} {
	find the actual translation memory file
} {

	set trados_folder "[im_trans_trados_folder -company_id $company_id]/TM"

	set language_string "[im_category_from_id -translate_p 0 $source_language_id]_*[im_category_from_id -translate_p 0 $target_language_id]"
	if {$subject_area_id ne ""} {
		set file_path [glob -nocomplain -directory $trados_folder "*$language_string*[im_category_from_id -translate_p 0 $subject_area_id]*"]
		if {$file_path eq ""} {
			set parent_source_language_id [lindex [im_category_parents $source_language_id] 0]
			set language_string "[im_category_from_id -translate_p 0 $parent_source_language_id]_*[im_category_from_id -translate_p 0 $target_language_id]"
			set file_path [glob -nocomplain -directory $trados_folder "*$language_string*[im_category_from_id -translate_p 0 $subject_area_id]*"]
		}
		if {$file_path eq ""} {
			set parent_target_language_id [lindex [im_category_parents $target_language_id] 0]
			set language_string "[im_category_from_id -translate_p 0 $parent_source_language_id]_*[im_category_from_id -translate_p 0 $parent_target_language_id]"
			set file_path [glob -nocomplain -directory $trados_folder "*$language_string*[im_category_from_id -translate_p 0 $subject_area_id]*"]
		}
		if {$file_path eq ""} {
			set file_path [im_trans_trados_find_tm -company_id $company_id -source_language_id $source_language_id -target_language_id $target_language_id]
		}
	} else {
		set file_path [glob -nocomplain -directory $trados_folder "*$language_string*"]

		if {$file_path eq ""} {
			set parent_source_language_id [lindex [im_category_parents $source_language_id] 0]
			if {$parent_source_language_id ne ""} {
				set language_string "[im_category_from_id -translate_p 0 $parent_source_language_id]_*[im_category_from_id -translate_p 0 $target_language_id]"
				set file_path [glob -nocomplain -directory $trados_folder "*$language_string*"]
			}
		}
		if {$file_path eq ""} {
			set parent_target_language_id [lindex [im_category_parents $target_language_id] 0]
			if {$parent_target_language_id ne "" && $parent_source_language_id ne ""} {
				set language_string "[im_category_from_id -translate_p 0 $parent_source_language_id]_*[im_category_from_id -translate_p 0 $parent_target_language_id]"
				set file_path [glob -nocomplain -directory $trados_folder "*$language_string*"]
			}
		}
	}

	return $file_path
}

ad_proc -public im_trans_trados_folder_component {
	{-company_id ""}
	{-project_id ""}
	{-user_id ""}
	{-return_url ""}
} {
	Display the TMs of a company or project
} {
	if {$company_id ne ""} {
		set params [list  [list base_url "/intranet-translation/"]  [list company_id $company_id] [list return_url [im_biz_object_url $company_id]]]
	} elseif {$project_id ne ""} {
		set params [list  [list base_url "/intranet-translation/"]  [list project_id $project_id] [list return_url [im_biz_object_url $project_id]]]
	}
	
	if {[exists_and_not_null params]} {
		set result [ad_parse_template -params $params "/packages/intranet-trans-trados/lib/trados-folder"]
	} else {
		set result ""
	}
}

# ---------------------------------------------------------------------
# Determine the list of missing files
# ---------------------------------------------------------------------

ad_proc im_trans_trados_missing_file_list {
	{-no_complain 0}
	project_id
} {
	Returns a list of task_ids that have not been found
	in the project folder.
	These task_ids can be used to display a list of
	files that the user has to upload to make the project
	workflow work without problems.
	The algorithm works O(n*log(n)), using ns_set, so
	it should be a reasonably cheap operation.

	@param no_complain Don't emit ad_return_complaint messages
		   in order not to disturb a users's page with missing pathes etc.

} {
	set find_cmd [im_filestorage_find_cmd]


	set project_path [cog::project::path -project_id $project_id]
	set source [lang::message::lookup "en_US" intranet-translation.Workflow_source_directory "source"]
	set missing_file_list [list]

	
	# ---------------------------------------------------------------
	# Build a list of files across all found source folders
	# ---------------------------------------------------------------
	foreach target_language [im_target_languages $project_id] {
		set source_folder "$project_path/${source}_$target_language"
		set target_language_id [db_string target_language "select category_id from im_categories where category = :target_language and category_type = 'Intranet Translation Language'"]
		
		set org_paths [split $source_folder "/"]
		set org_paths_len [llength $org_paths]
		if { [catch {
			set find_cmd [im_filestorage_find_cmd]
			set file_list [exec $find_cmd $source_folder -type f]
		} err_msg] } {
			# The directory probably doesn't exist yet, so don't generate
			# an error !!!
		
			if {$no_complain} { return "" }
			ad_return_complaint 1 "im_task_missing_file_list: directory $source_folder<br>probably does not exist:<br>$err_msg"
			set file_list ""
		}
		
		# Get the sorted list of files in the directory
		set files [split $file_list "\n"]

		set file_set [ns_set create]

		# Get all tasks for this language
		foreach file $files {
		
			# Get the basic information about a file
			set file_paths [split $file "/"]
			set len [expr [llength $file_paths] - 1]
			set file_comps [lrange $file_paths $org_paths_len $len]
			set file_name [join $file_comps "/"]
			
			# Check if it is the toplevel directory
			if {[string equal $file $project_path]} {
				# Skip the path itself
				continue
			}

			ns_set put $file_set $file_name $file_name
			ns_log Debug "im_task_missing_file_list: file_name=$file_name"
		}
		
		# We've got now a list of all files in the source folders.
		# Let's go now through all the im_trans_tasks of this project and
		# check if the filename present in the $file_list
		# Attention!, this is an n^2 algorithm!
		# Any ideas how to speed this up?
		
		set task_sql "
		select
			task_id,
			task_name,
			task_filename
		from
			im_trans_tasks t
		where
			t.project_id = :project_id
			and t.target_language_id = :target_language_id
		"
		
		db_foreach im_task_list $task_sql {
	
			if {"" != $task_filename} {
				set res [ns_set get $file_set $task_filename]
				if {"" == $res} {
					# We haven't found the file
					# Check with sdlxliff ending
					set res [ns_set get $file_set ${task_filename}.sdlxliff]
					if {"" == $res} {
						lappend missing_file_list $task_id
						ds_comment "$target_language :: $task_filename :: $task_id"
					}
				}
			}
	
		}
	
		ns_set free $file_set
	}
	return $missing_file_list
}

ad_proc -public im_trans_trados_setup_directory {
	-project_id
} {
	Setup the folder Structur according to Trados 2015
	
	- One Directory per Language involved. This will be used for both source as well as target files
	- Packages Directory for the "IN" and "OUT" packages.
	- Reports Directory to store the analysis
	- SDLPROJ File and all TB/TMs for this project
	
	@project_id Project ID for which the folder will be created
} {
	set locale "en_US"
	
	# Get some missing variables about the project and the company
	set query "
	select
		p.project_type_id,
		p.project_path,
		p.company_id,
		p.final_company_id,
		im_category_from_id(p.source_language_id) as source_language,
		source_language_id,
		subject_area_id,
		project_nr,
		project_name,
		c.company_path
	from
		im_projects p,
		im_companies c
	where
		p.project_id = :project_id
		and p.company_id = c.company_id
	"
	if { ![db_0or1row projects_info_query $query] } {
		return "Can't find the project with group id of $project_id"
	}

    set trados_directories [list "Originals" "Reports" $source_language]

    # Define the windows path. If we have a trados server configured use that, otherwise use the company win path
    if {[im_trans_trados_configured_p]} {
		set trados_win_path [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosWinPath"]
		lappend trados_directories "TM"
		lappend trados_directories "TB"
		lappend trados_directories "Packages"
    } else {
		set trados_win_path [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosCompanyWinPath"]
    }
		
	# Create the project dir if it doesn't already exist
	set project_dir [cog::project::path -project_id $project_id]
	
	im_filestorage_mkdir -dir ${project_dir}/Trados
	
	# Create the various subdirectories directory
	foreach target_language [im_target_languages $project_id] {
		lappend trados_directories $target_language
	}
	
	foreach trados_dir $trados_directories {
		ns_log Debug "im_trans_trados_setup_directory: trados_dir=${project_dir}/Trados/$trados_dir"
		im_filestorage_mkdir -dir ${project_dir}/Trados/$trados_dir
	}
	
	
	# ---------------------------------------------------------------
	# Create and download the sdlproj
	# ---------------------------------------------------------------
	set project_name_xml [ad_text_to_html $project_name]
	
	# Only use one sdltpl file for the time being
	set trados_sdlproj "[acs_package_root_dir intranet-trans-trados]/lib/PT.sdlproj"
		
	# Calculate the GUID for the Project and append missing 0
	set guid $project_id
	set len [string length $project_id]
	set missing_frac [expr 8-$len]
	while {$missing_frac > 0} {
		set guid "${guid}0"
		set missing_frac [expr $missing_frac-1]
	}
	
	# Create the language mappings
	set target_language_ids [im_target_language_ids $project_id]
	
	set language_mapping_xml ""
	set language_directions_xml "<LanguageDirections>"
	set language_settings_bundle_guid "454df948-fb6e-44d7-8d7c-0426fca7a774"
	
	foreach target_language_id $target_language_ids {
	
		# Calculate the GUID for the Language and append missing 0
		set language_guid $target_language_id
		set len [string length $target_language_id]
		set missing_frac [expr 8-$len]
		while {$missing_frac > 0} {
			set language_guid "${language_guid}0"
			set missing_frac [expr $missing_frac-1]
		}
	
		append language_directions_xml "
		  <LanguageDirection Guid=\"${language_guid}-e496-4c9c-be52-afb515a59023\" SettingsBundleGuid=\"$language_settings_bundle_guid\" TargetLanguageCode=\"[im_category_from_id -translate_p 0 $target_language_id]\" SourceLanguageCode=\"$source_language\">
			<AutoSuggestDictionaries />
			<CascadeItem OverrideParent=\"false\" StopSearchingWhenResultsFound=\"false\" />
		  </LanguageDirection>"
	}
	append language_directions_xml "</LanguageDirections>"
	
	# Create a "unique" start_date
	set start_date [db_string test "select to_char(now(),'YYYY-MM-DD HH24:MM:SS') from dual"]
	set start_date [join $start_date "T"]
	append start_date ".7767411Z"
	
	
	# ---------------------------------------------------------------
	# Prepare the templates for Termbases
	# ---------------------------------------------------------------
	set file_path ""

	# Check if we have a final customer's TB
	if {$final_company_id ne ""} {
		set trados_folder "[im_trans_trados_folder -company_id $final_company_id]/TB"
		set file_name [im_trans_trados_filename \
			-company_id $final_company_id \
			-extension ".sdltb"
		]
	
		# Check if we have one for the customer
		if {[file exists ${trados_folder}/$file_name] && $file_name ne ""} {
			set file_path ${trados_folder}/$file_name
			set company_path [db_string path "select company_path from im_companies where company_id = :final_company_id"]
		}
	}
	
	if {$file_path eq ""} {
		# Check if we have one for the customer
		set trados_folder "[im_trans_trados_folder -company_id $company_id]/TB"
		set file_name [im_trans_trados_filename \
			-company_id $company_id \
			-extension ".sdltb"
		]

		# Check if we have one for the customer
		if {[file exists ${trados_folder}/$file_name] && $file_name ne ""} {
		    set file_path ${trados_folder}/$file_name
		    set company_path [db_string path "select company_path from im_companies where company_id = :company_id"]
		}
	}
	
	if {$file_path ne "" && 0} {
		
	    set tb_name [intranet_termbase::multiterm::friendly_name -tb_path $file_path]

		# Get the last part of the path to retrieve the filename
		set file_paths [split $file_path "/"]
		set file_paths_len [llength $file_paths]
		set file_name [lindex $file_paths [expr $file_paths_len -1]]
	    set trados_tb_win "$trados_win_path\\$company_path\\TB"
	    
		set termbase_xml "<Termbases>
			  <Name>[ad_text_to_html $tb_name]</Name>
			  <SettingsXml>&lt;?xml version=\"1.0\" encoding=\"utf-16\"?&gt;
		&lt;TermbaseSettings xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"&gt;
		  &lt;Path&gt;$trados_tb_win\\$file_name&lt;/Path&gt;
		  &lt;IsOpen&gt;false&lt;/IsOpen&gt;
		  &lt;Filter&gt;0&lt;/Filter&gt;
		  &lt;FilterHighlight&gt;false&lt;/FilterHighlight&gt;
		  &lt;Layout&gt;0&lt;/Layout&gt;
		  &lt;Local&gt;true&lt;/Local&gt;
		&lt;/TermbaseSettings&gt;</SettingsXml>
			  <Enabled>true</Enabled>
			</Termbases>
			"
	
		set locales [im_target_languages $project_id]
		lappend locales $source_language
	
	    # Get the mappings from the tb_file
	    im_trans_trados_tb_language_mapping -tb_path $file_path -array_name tb_languages
	    foreach locale $locales {
		set language ""
		if {[exists_and_not_null tb_languages($locale)]} {
		    set language $tb_languages($locale)
		} else {
		    # Try with the iso only
		    set locale [lindex [split $locale "-"] 0]
		    if {[exists_and_not_null tb_languages($locale)]} {
			set language $tb_languages($locale)
		    }
		}
		
		if {$language ne ""} {
		    append termbase_xml "
				<LanguageIndexMappings>
					  <Language>$locale</Language>
					  <Index>$language</Index>
					</LanguageIndexMappings>"
		}
	    }
	} else {
		set termbase_xml ""
	}
	
	# ---------------------------------------------------------------
	# Prepare the Translation Memories
	# ---------------------------------------------------------------
	
	set tm_xml ""
	
	foreach target_language_id $target_language_ids {
	
		# Find the correct translation memories
		foreach file_path [im_trans_trados_get_trans_memory_file \
			-company_id $company_id \
			-final_company_id $final_company_id \
			-source_language_id $source_language_id \
			-target_language_id $target_language_id \
			-subject_area_id $subject_area_id] {
			
			# Copy the template to the trados folder of the working_dir
			set file_paths [split $file_path "/"]
			set file_paths_len [llength $file_paths]
			
			# Get the last part of the path to retrieve the filename
			set file_name [lindex $file_paths [expr $file_paths_len -1]]
			
			# Get the company_path from the file len.
			set company_path [lindex $file_paths [expr $file_paths_len -3]]
		    if {[im_trans_trados_configured_p]} {
			file copy -force $file_path "${project_dir}/Trados/TM"
			append tm_xml "<CascadeEntryItem PerformConcordanceSearch=\"true\" Penalty=\"0\" PerformUpdate=\"true\" PerformNormalSearch=\"true\">
			  <MainTranslationProviderItem Uri=\"sdltm.file:///TM/$file_name\" Enabled=\"true\" />
		</CascadeEntryItem>"
		    } else {
			regsub -all {\\} $trados_win_path {/} trados_tm_path
			set trados_dir_win "$trados_tm_path/$company_path"
			append tm_xml "<CascadeEntryItem PerformConcordanceSearch=\"true\" Penalty=\"0\" PerformUpdate=\"true\" PerformNormalSearch=\"true\">
			  <MainTranslationProviderItem Uri=\"sdltm.file:///$trados_dir_win/TM/$file_name\" Enabled=\"true\" />
		</CascadeEntryItem>"
		    }
		}
	}
	
	# ---------------------------------------------------------------
	# Finalize and write the sdlproj
	# ---------------------------------------------------------------
	set file [open "$trados_sdlproj"]
	fconfigure $file -encoding "utf-8"
	set sdlproj_template [read $file]
	close $file
	
	eval [template::adp_compile -string $sdlproj_template]
	set sdlproj $__adp_output
	
	# write the file to disk
	set return_sdlproj "${project_dir}/Trados/${project_nr}.sdlproj"
	set file [open $return_sdlproj w]
	fconfigure $file -encoding "utf-8"
	puts $file $sdlproj
	flush $file
	close $file
	
	# ---------------------------------------------------------------
	# Sync the project_dir to the windows folder if necessary
	# ---------------------------------------------------------------

	im_trans_trados_project_info -project_id $project_id -array_name project_info
	
	set project_dir $project_info(project_dir)
	set trados_dir $project_info(trados_dir)
	
	if {"${project_dir}/Trados" ne $trados_dir} {
		im_filestorage_mkdir -dir $trados_dir
		im_filestorage_rsync -source_path ${project_dir}/Trados -target_path $trados_dir
	}
}


ad_proc -public im_trans_trados_new_file_p {
	-project_id
} {
	Checks if we have new TM for the project
} {
    if {[im_trans_trados_configured_p]} {
	set project_query "
		select
			p.project_nr,
			p.source_language_id,
			company_id,
			final_company_id,
			subject_area_id
		from
			im_projects p
		where
			p.project_id=:project_id
	"
	
	if { ![db_0or1row projects_info_query $project_query] } {
		ad_return_complaint 1 "[_ intranet-translation.lt_Cant_find_the_project]"
		return
	}

	set project_dir [cog::project::path -project_id $project_id]
	
	set new_files_p 0
	
	
	# ---------------------------------------------------------------
	# Check new TMs
	# ---------------------------------------------------------------
	
	set tm_xml ""
	set target_language_ids [im_target_language_ids $project_id]
	
	foreach target_language_id $target_language_ids {
	
		# Find the correct translation memories
		foreach tm_path [im_trans_trados_get_trans_memory_file \
			-company_id $company_id \
			-final_company_id $final_company_id \
			-source_language_id $source_language_id \
			-target_language_id $target_language_id \
			-subject_area_id $subject_area_id] {
	
			set file_paths [split $tm_path "/"]
			set file_paths_len [llength $file_paths]
	
			# Get the last part of the path to retrieve the filename
			set file_name [lindex $file_paths [expr $file_paths_len -1]]
			set project_tm_path "${project_dir}/Trados/TM/$file_name"
			if {[file mtime $tm_path]>[file mtime $project_tm_path]} {
				set new_files_p 1
			}
		}
	}
    } else {
	set new_files_p 0
    }

    return $new_files_p
}

ad_proc -public im_trans_trados_project_p {
	-project_id
} {
	Returns 1 if this is a trados project. Will decide based on the sdlproj in the project directory
} {
	set trados_dir [im_trans_trados_project_folder -project_id $project_id]
	set sdlproj_file [glob -nocomplain -join -dir ${trados_dir} *.sdlproj]
	if {$sdlproj_file eq ""} {
		return 0
	} else {
		return 1
	}
}

ad_proc -public im_trans_trados_package_file {
	-target_language_id
	-project_id
	-task_type
} {
	Check if we have a package in the trados out folder
} {
	im_trans_trados_project_info -project_id $project_id -array_name project_info
	set target_language [im_category_from_id -translate_p 0 $target_language_id]
	set trados_dir $project_info(trados_dir)
		
	switch $task_type {
		trans {
			set glob_filter "$project_info(project_nr)*${target_language}-*sdlppx"
			if {[catch {glob -directory "${trados_dir}/Packages/Out" $glob_filter} out_files]} {
				# Try wit the Master locale
				set main_language [lindex [split $target_language "-"] 0]
				set glob_filter "$project_info(project_nr)*\{${main_language},[string toupper $main_language]\}-*sdlppx"
				if {[catch {glob -directory "${trados_dir}/Packages/Out" $glob_filter} out_files]} {
					set out_files [list]
				}
			}
		}
		proof {
			set glob_filter "$project_info(project_nr)*${target_language}*KORR*sdlppx"
			if {[catch {glob -directory "${trados_dir}/Packages/Out" $glob_filter} out_files]} {
				# Try wit the Master locale
				set main_language [lindex [split $target_language "-"] 0]
				set glob_filter "$project_info(project_nr)*\{${main_language},[string toupper $main_language]\}*KORR*sdlppx"
				if {[catch {glob -directory "${trados_dir}/Packages/Out" $glob_filter} out_files]} {
					set out_files [list]
				}
			}
		}
	    default {
		set glob_filter ""
		set out_files [list]
	    }
	}
	
	ds_comment "$task_type :: $glob_filter :: $out_files"
	# Get the latest file
	set modification_date ""
	set package_out_file ""
	
	foreach out_file $out_files {
			set mod [file atime $out_file]
			if {$mod>$modification_date} {
					set modification_date $mod
					set package_out_file $out_file
			}
	}

	return $package_out_file
}

ad_proc -public im_trans_trados_packages {
	-target_language_id
	-project_id
	-task_type
} {
	returns a list of list with all the package files found and the documents contained in them
} {
	im_trans_trados_project_info -project_id $project_id -array_name project_info
	set target_language [im_category_from_id -translate_p 0 $target_language_id]
	set trados_dir $project_info(trados_dir)

	switch $task_type {
		trans {
			set glob_filter "$project_info(project_nr)*${target_language}-*sdlppx"
			if {[catch {glob -directory "${trados_dir}/Packages/Out" $glob_filter} out_files]} {
				# Try wit the Master locale
				set main_language [lindex [split $target_language "-"] 0]
				set glob_filter "$project_info(project_nr)*\{${main_language},[string toupper $main_language]\}-*sdlppx"
				if {[catch {glob -directory "${trados_dir}/Packages/Out" $glob_filter} out_files]} {
					set out_files [list]
				}
			}
		}
		proof {
			set glob_filter "$project_info(project_nr)*${target_language}*KORR*sdlppx"
			if {[catch {glob -directory "${trados_dir}/Packages/Out" $glob_filter} out_files]} {
				# Try wit the Master locale
				set main_language [lindex [split $target_language "-"] 0]
				set glob_filter "$project_info(project_nr)*\{${main_language},[string toupper $main_language]\}*KORR*sdlppx"
				if {[catch {glob -directory "${trados_dir}/Packages/Out" $glob_filter} out_files]} {
					set out_files [list]
				}
			}
		}
		default {
			set glob_filter ""
			set out_files [list]
		}
	}
	set package_info_list [list]
	foreach out_file $out_files {
		set task_ids [list]
		set files_in_package_list [split [exec -- /usr/bin/unzip -Z -1 "$out_file"] "\n"]
		foreach file $files_in_package_list {
			if {[string match "${target_language}/*.sdlxliff" $file]} {
				# Substract .sdlxliff at the end
				set source_file [string range [lindex [split $file "/"] 1] 0 end-9]
				set task_id [db_string task "select task_id from im_trans_tasks where task_filename=:source_file and project_id = :project_id and target_language_id = :target_language_id" -default ""]
				if {$task_id eq ""} {
				    ds_comment "Missing task for $source_file in $out_file"
				} else {
				    lappend task_ids $task_id
				}
			}
		}
		lappend package_info_list [list $out_file $task_ids]
	}
	return $package_info_list
}


ad_proc -public im_trans_trados_update_project_tm {
    {-project_id ""}
    {-project_ids ""}

} {
	Update the TM  for the project
} {

    if {$project_ids eq ""} {
	set project_ids [list $project_id]
    }

    foreach project_id $project_ids {
	db_1row project_query "
		select
			p.project_nr,
			p.source_language_id,
			p.project_type_id,
			company_id,
			final_company_id,
			subject_area_id
		from
			im_projects p
		where
			p.project_id=:project_id
	"
    if {$source_language_id eq ""} {
	return
    } else {
	ns_log Debug "trans_trads_update_project_tm :: Update $project_id"
	im_trans_trados_project_info -project_id $project_id -array_name project_info
		

	set project_dir $project_info(project_dir)
	set trados_dir $project_info(trados_dir)
		
		
	set source_language [im_name_from_id $source_language_id]
		
	# ---------------------------------------------------------------
	# Update the TMs
	# ---------------------------------------------------------------
	
	foreach target_language_id [im_target_language_ids $project_id] {
		# Get the expected TMs for the target language
	
		set tm_paths [im_trans_trados_get_trans_memory_file \
				-company_id $company_id \
				-final_company_id $final_company_id \
				-source_language_id $source_language_id \
				-target_language_id $target_language_id \
				-subject_area_id $subject_area_id]
	
		foreach file_path $tm_paths {
			# Check if the file exists
			set filename "[lindex [split [file rootname $file_path] "/"] end].sdltm"
			if {[file exists "${trados_dir}/TM/$filename"]} {
				ns_log Debug "Updating TM $file_path from project $project_nr"
				# Upload the new TM
				im_trans_trados_update_tm \
					-current_tm $file_path \
					-new_tm ${trados_dir}/TM/$filename \
					-project_id $project_id
					
				# Rename the file so we won't update it again
				file rename "${trados_dir}/TM/$filename" "${trados_dir}/TM/Finalized_$filename"
			}
		}
	}
    }
    }
}

ad_proc -public im_trans_trados_update_projects_tm {
    -last_successful_project_nr
} {
    Updates the TMS since the last successful project_nr
} {
    set closed_ids [im_sub_categories 81]

    set project_ids [list]
    foreach project_id [db_list list "select project_id from im_projects where source_language_id is not null and end_date >= (select end_date from im_projects where project_nr =:last_successful_project_nr) and project_status_id in ([ns_dbquotelist $closed_ids]) order by end_date, project_nr asc"] {
	if {[im_trans_trados_project_p -project_id $project_id]} {
lappend project_ids $project_id
	}
    }

    util_background_exec -pass_vars project_ids -name malte {im_trans_trados_update_project_tm -project_ids $project_ids}
}


ad_proc -public im_trans_trados_migrate_folder {
    {-old_TradosUnixPath ""}
    {-new_TradosUnixPath ""}
} {
    Updates the folder for the company
} {
    if {$old_TradosUnixPath eq ""} {
	set old_TradosUnixPath [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosUnixPath"]
    }
    if {$new_TradosUnixPath eq ""} {
	set new_TradosUnixPath [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosCompanyUnixPath"]
    }

    set customer_or_internal_type_ids [im_sub_categories -include_disabled_p 1 10246]
    lappend customer_or_internal_type_ids 10247

    db_foreach company "select company_path, company_id, company_type_id from im_companies order by company_id" {
	set old_trados_folder "$old_TradosUnixPath/$company_path/trados"
	if {[file exists $old_trados_folder]} {
	    if {[lsearch $customer_or_internal_type_ids $company_type_id]>-1} {
		# This is a customer, migrate the directory
		set new_trados_folder "$new_TradosUnixPath/$company_path"
		if {![file exists $new_trados_folder]} {
		    file mkdir $new_trados_folder
		    file mkdir "$new_trados_folder/TM"
		    file mkdir "$new_trados_folder/TB"
		}
		
		# Now Move files correctly
		foreach trados_file [glob -nocomplain "$old_trados_folder/*"] {
		    switch [file extension $trados_file] {
			.sdltm {
			    catch {file rename $trados_file "$new_trados_folder/TM"}
			    ns_log Debug "Moved $trados_file into $new_trados_folder/TM"
			} 
			.sdltb {
			    catch {file rename $trados_file "$new_trados_folder/TB"}
			    ns_log Debug "Moved $trados_file into $new_trados_folder/TB"
			}
			default {
			    file delete -force $trados_file
			}
		    }
		}

		file delete -force $old_trados_folder

	    } else {
		# This is not a customer, should not have a trados folder to begin with
		file delete -force $old_trados_folder
		ns_log Debug "Removed trados folder for $company_path"
	    }
	}
    }    
}


ad_proc -public im_trans_trados_tb_language_mapping {
    -tb_path
    -array_name
} {
	Returns the list of languages supported and sets the array
	to return the language_id for the language

	@return List of language_ids if successfully able to map all language, 0 otherwise
} {

    upvar 1 $array_name tb_array
    if {[info exists tb_array]} {
	unset tb_array
    }

    # Catch as mdb-export does not work on mounted drives easily
    if { [catch {exec /usr/bin/mdb-export $tb_path mtIndexes} mtIndexes_csv] } {
	set tmp_name "/tmp/[file tail $tb_path]"
	file copy -force $tb_path $tmp_name
	set mtIndexes_csv [exec /usr/bin/mdb-export $tmp_name mtIndexes]
	file delete $tmp_name
    }
		
    # Let's assume all goes fine
    set category_type "Intranet Translation Language"
    set language_ids [list]
    foreach line "[im_csv_get_values $mtIndexes_csv ,]" {
	set lang [string toupper [lindex $line 1]]
	set type [lindex $line 0]
	if {$lang ne "" || $type ne ""} {
	    set category_id [db_string language "select category_id from im_categories where category_type = :category_type and aux_string1 = :type and enabled_p = 't' limit 1" -default ""]
	    if {$category_id eq ""} {
		# try to find the language_id using the lang
		set category_id [db_string language "select category_id from im_categories where category_type = :category_type and upper(category) = :lang" -default ""]
	    }
	    if {$category_id eq ""} {
		# Maybe the language actually is a type
		set category_id [db_string language "select category_id from im_categories where category_type = :category_type and upper(category) = :type" -default ""]
	    }
	    if {$category_id eq ""} {
		# Try with the iso only
		set category_id [db_string language "select category_id from im_categories where category_type = :category_type and upper(category) like '${lang}-' order by sort_order limit 1" -default ""]
	    }
	    if {$category_id eq ""} {
		# Try with the iso only
		set category_id [db_string language "select category_id from im_categories where category_type = :category_type and upper(category) like '-${lang}' order by sort_order limit 1" -default ""]
	    }
	    if {$category_id eq ""} {
		# We did not match a language, therefore create it
		set category_id [db_string max_cat_id "select max(category_id) from im_categories" -default 10000]
		set category_id [expr $category_id + 1]
		
		# ---------------------------------------------------------------
		# Update the category
		# ---------------------------------------------------------------
		
		db_dml new_category_entry {
		    insert into im_categories (category_id, category, category_type) values (:category_id, :lang, :category_type)
		}
					
		# set aux_string1
		db_dml update "update im_categories set aux_string1 = :type where category_id = :category_id"
		callback im_category_after_create -object_id $category_id \
		    -status "" -type "" -category_id $category_id \
		    -category_type $category_type
		
		ns_log Debug "Created category for language $type and iso-code $lang"
	    }
	    
	    if {$category_id ne ""} {
		set language_id $category_id
		set tb_array($type) $language_id
		set tb_array($lang) $language_id
		set tb_array([im_category_from_id -translate_p 0 $language_id]) $language_id
		lappend language_ids $language_id
	    }
	}
    }
    return $language_ids
}
