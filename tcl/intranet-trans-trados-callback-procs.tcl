# /packages/intranet-trans-trados/tcl/intranet-trans-trados-callback-procs.tcl


ad_library {
	Callbacks for trados
	
	@author malte.sussdorff@cognovis.de
}

ad_proc -public -callback im_project_after_create -impl trans-trados {
	{-object_id ""}
	{-status_id ""}
	{-type_id ""}
} {
	Create the trados folder for the project
} {
    
    ns_log Debug "Callback to create trados directories"
    
    # Only do this for translation projects
    set translation_type_ids [im_sub_categories [im_project_type_translation]]
    
    if {[lsearch $translation_type_ids $type_id]>-1} {

		# Check if the project type has a trans workflow in it.
		set workflow_steps [db_string workflow_steps "select aux_string1 from im_categories where category_id = :type_id" -default ""]
		if {[lsearch $workflow_steps "trans"] >-1} { 
			im_trans_trados_setup_directory -project_id $object_id
		}
    }
}
