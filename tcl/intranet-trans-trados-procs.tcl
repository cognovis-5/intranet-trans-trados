# /packages/intranet-trans-trados/tcl/intranet-trans-trados-procs.tcl


ad_library {
    Bring together all "components" (=HTML + SQL code)
    related to the Translation sector for TRADOS

	@author malte.sussdorff@cognovis.de
    @author frank.bergmann@project-open.com
    @author juanjoruizx@yahoo.es
}

ad_proc -public im_package_trans_trados_id {} {
	Returns the package id of the intranet-trans-trados package
} {
	return [util_memoize im_package_trans_trados_id_helper]
}

ad_proc -private im_package_trans_trados_id_helper {} {
	return [db_string im_package_core_id {
	select package_id from apm_packages
	where package_key = 'intranet-trans-trados'
	} -default 0]
}


ad_proc im_trans_trados_remove_sdlxliff {} {
    Remove the sdlxliff extension from the filename
    
} {
    db_foreach tasks {select task_name, task_id from im_trans_tasks where task_name like '%sdlxliff'} {
       set task_name [string trimright $task_name "sdlxliff"]
       set task_name [string range $task_name 0 end-1]
       catch {db_dml update "update im_trans_tasks set task_name = :task_name where task_id = :task_id"}
    }
    db_foreach tasks {select task_filename, task_id from im_trans_tasks where task_filename like '%sdlxliff'} {
       set task_filename [string trimright $task_filename "sdlxliff"]
       set task_filename [string range $task_filename 0 end-1]
       catch {db_dml update "update im_trans_tasks set task_filename = :task_filename where task_id = :task_id"}
    }
}


ad_proc -public im_trans_trados_create_tasks {
	{-project_id:required}
	{-trados_analysis_xml:required}
	{-target_language_id ""}
	{-filename ""}
} {
	Create translation tasks from trados analysis
} {
    set created_task_ids ""

	# ---------------------------------------------------------------------
	# Get some more information about the project
	# ---------------------------------------------------------------------
	
	set project_query "
		select
			p.project_nr as project_short_name,
			p.company_id,
			c.company_name as company_short_name,
			p.source_language_id,
			p.project_type_id,
			p.project_lead_id
		from
			im_projects p
			  LEFT JOIN
			im_companies c USING (company_id)
		where
			p.project_id=:project_id
	"
	if { ![db_0or1row projects_info_query $project_query] } {
		ad_return_complaint 1 "[_ intranet-translation.lt_Cant_find_the_project]"
		return
	}

	if {$target_language_id eq ""} {
		foreach target_language [im_target_languages $project_id] {
			if {[string match "*${target_language}*" $trados_analysis_xml]} {
				set target_language_id [cog_category_id -category $target_language -category_type "Intranet Translation Language"]
				break
			}
		}
	}
	
	
	# Setup the elements to search for and how to map them
	keylset element_keyl tag "perfect"
	keylset element_keyl variable "match_perf"
	set single_elements [list $element_keyl]
	
	keylset element_keyl tag "inContextExact"
	keylset element_keyl variable "match_x"
	lappend single_elements $element_keyl
	
	keylset element_keyl tag "exact"
	keylset element_keyl variable "match100"
	lappend single_elements $element_keyl

	keylset element_keyl tag "new"
	keylset element_keyl variable "match0"
	lappend single_elements $element_keyl	

	keylset element_keyl tag "locked"
	keylset element_keyl variable "locked"
	lappend single_elements $element_keyl
	
	keylset element_keyl tag "repeated"
	keylset element_keyl variable "match_rep"
	lappend single_elements $element_keyl
	
	keylset element_keyl tag "crossFileRepeated"
	keylset element_keyl variable "match_cfr"
	lappend single_elements $element_keyl
	
	# ---------------------------------------------------------------
	# Get the XML Analysis
	# ---------------------------------------------------------------

	set xml [new_CkXml]
	
	set success [CkXml_LoadXmlFile $xml $trados_analysis_xml]
	if {[expr $success != 1]} then {
		ns_log Error [CkXml_lastErrorText $xml]
		delete_CkXml $xml
		exit
	}
	CkXml_put_Encoding $xml "utf-8"
	CkXml_put_Utf8 $xml 1

	# ---------------------------------------------------------------
	# Loop through all files which were analysed
	# ---------------------------------------------------------------
	
	set numFiles [CkXml_NumChildrenHavingTag $xml "file"]
	set variables_for_array [list match_x match_rep match100 match95 match85 match75 match50 match0 \
match_perf match_cfr match_f95 match_f85 match_f75 match_f50 locked]
	set file_names [list]
	
	for {set i 0} {$i <= [expr $numFiles - 1]} {incr i} {
		set success [CkXml_GetNthChildWithTag2 $xml "file" $i]
		set file_name [CkXml_getAttrValue $xml name]
		
		ns_log Debug "Trados Filename :: $file_name in  $trados_analysis_xml"
		# If we only need one file, then check the filename
		if {$filename ne ""} {
			if {![string match "*${filename}*" $file_name]} {
				ns_log Debug "Found $file_name but I am looking for $filename"
				CkXml_GetParent2 $xml ; # file
				continue
			}
		}
		
		# Skip already processed files
		if {[lsearch $file_names $file_name]<0} {
			lappend file_names $file_name
		} else {
			continue
		}
		
		# Go into the analysis
		CkXml_FindChild2 $xml "analyse"
		
		# Loop through all single elements to get the words
		foreach element_keyl $single_elements {
			CkXml_FindChild2 $xml [keylget element_keyl tag]
			set [keylget element_keyl variable] [CkXml_getAttrValue $xml words]
			CkXml_GetParent2 $xml ; # element
			
			set variable [keylget element_keyl variable]
		}
		
		# Loop through all the fuzzy elements and get the words
		set numFuzzy [CkXml_NumChildrenHavingTag $xml "fuzzy"]
		for {set k 0} {$k <= [expr $numFuzzy - 1]} {incr k} {
			CkXml_GetNthChildWithTag2 $xml "fuzzy" $k
			set min [CkXml_getAttrValue $xml min]
			set match$min [CkXml_getAttrValue $xml words]
			ds_comment "match$min :: [set match$min]"
			CkXml_GetParent2 $xml ; # fuzzy
		}

		# Loop through all the internalfuzzy elements and get the words
		# they are optional, set the variables just in case
		foreach percent [list 50 75 85 95] {
			set match_f$percent 0
		}
		set numFuzzy [CkXml_NumChildrenHavingTag $xml "internalfuzzy"]
		for {set k 0} {$k <= [expr $numFuzzy - 1]} {incr k} {
			CkXml_GetNthChildWithTag2 $xml "internalfuzzy" $k
			set min [CkXml_getAttrValue $xml min]
			set match_f$min [CkXml_getAttrValue $xml words]
			ds_comment "match_f$min :: [set match_f$min]"
			CkXml_GetParent2 $xml ; # fuzzy
		}
		
		CkXml_GetParent2 $xml ; # analyse
		CkXml_GetParent2 $xml ; # file
		
		# Set the variables in the array for later reuse
		foreach variable $variables_for_array {
		    if {[info exists $variable]} {
			set ${variable}_arr($file_name) [set $variable]
		    } else {
			set ${variable}_arr($file_name) ""
		    }
		}
		
	}
	
	ns_log Debug "Task analysis given $filename for $file_names in [im_category_from_id $target_language_id]"

	# Now loop through all files
	foreach file_name $file_names {
		
		# Take the array variables back :-)
		foreach variable $variables_for_array {
			set $variable [set  ${variable}_arr($file_name)]
		}
		
	 	# ---------------------------------------------------------------
		# Insert / Update the task
		# ---------------------------------------------------------------

		# Remove the sdlxliff
		set file_name [string trimright $file_name "sdlxliff"]
		set file_name [string range $file_name 0 end-1]
		set task_name $file_name
		
		set task_units [im_trans_trados_matrix_calculate [im_company_freelance] \
			$match_x $match_rep $match100 $match95 $match85 $match75 $match50 $match0 \
			$match_perf $match_cfr $match_f95 $match_f85 $match_f75 $match_f50 $locked]
		
		# Determine the "billable_units" form the project's customer:
		set billable_units [im_trans_trados_matrix_calculate $company_id \
			$match_x $match_rep $match100 $match95 $match85 $match75 $match50 $match0 \
			$match_perf $match_cfr $match_f95 $match_f85 $match_f75 $match_f50 $locked]
					
		
		# Inter-Company invoicing enabled?
		set interco_p [parameter::get_from_package_key -package_key "intranet-translation" \
		 	-parameter "EnableInterCompanyInvoicingP" -default 0]
		set billable_units_interco $billable_units
		if {$interco_p} {
			set interco_company_id [db_string get_interco_company "select interco_company_id from im_projects where project_id=$project_id" -default ""]
			if {"" == $interco_company_id} { 
				set interco_company_id $company_id 
			}
			set billable_units_interco [im_trans_trados_matrix_calculate $interco_company_id \
				$match_x $match_rep $match100 $match95 $match85 $match75 $match50 $match0 \
				$match_perf $match_cfr $match_f95 $match_f85 $match_f75 $match_f50 $locked]
		}
	
		set task_id [db_string task "select task_id from im_trans_tasks where task_name = :task_name and project_id = :project_id and target_language_id = :target_language_id" -default ""]
		
		set task_status_id 340
		
		if {$task_id eq ""} {
			set task_id [im_exec_dml new_task "im_trans_task__new (
				null,			-- task_id
				'im_trans_task',	-- object_type
				now(),			-- creation_date
				:project_lead_id,		-- creation_user
				'0.0.0.0.',		-- creation_ip	
				null,			-- context_id	
				:project_id,		-- project_id	
				:project_type_id,		-- task_type_id	
				:task_status_id,	-- task_status_id
				:source_language_id,	-- source_language_id
				:target_language_id,	-- target_language_id
				324		-- task_uom_id, source words
				)"]
			set action create
		} else {
			set action update
		}
	
		db_dml update_task "
			UPDATE im_trans_tasks SET
			tm_integration_type_id = [im_trans_tm_integration_type_external],
			task_name = :task_name,
			task_filename = :task_name,
			task_units = :task_units,
			billable_units = :billable_units,
			billable_units_interco = :billable_units_interco,
			match_x = :match_x,
			match_rep = :match_rep,
			match100 = :match100, 
			match95 = :match95,
			match85 = :match85,
			match75 = :match75, 
			match50 = :match50,
			match0 = :match0,
			match_perf = :match_perf,
			match_cfr = :match_cfr,
			match_f95 = :match_f95,
			match_f85 = :match_f85,
			match_f75 = :match_f75,
			match_f50 = :match_f50,
			locked = :locked
			WHERE 
			task_id = :task_id
			"
	
	
		# Successfully created translation task
		# Call user_exit to let TM know about the event
		im_user_exit_call trans_task_$action $task_id
		cog::callback::invoke -object_type "im_trans_task" -action after_$action -object_id $task_id -status_id $task_status_id -type_id $project_type_id
		lappend created_task_ids $task_id
	}
	return $created_task_ids
}
