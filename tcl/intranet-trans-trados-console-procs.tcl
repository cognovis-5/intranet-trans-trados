# /packages/intranet-trans-trados/tcl/intranet-trans-trados-file-procs.tcl


ad_library {
    Bring together all file operations
    related to the Translation sector for TRADOS

	@author malte.sussdorff@cognovis.de
}

ad_proc im_trans_trados_console_exec {
	-config_file
} {
	Runs the console on windows with the config file name
	
	@param config_file Name of the config file with the windows path
} {
	# Figure out if we can even run the analysis and set the parameters
	set trados_console_app [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosConsoleApp"]
	set trados_server [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosServer"]
	set trados_user [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosUser"]
	set trados_port [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosServerSSHPort"]
	set private_key [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosServerSSHKey"]

	if {$trados_console_app eq "" || $trados_server eq "" || $trados_port eq ""} {
		    acs_mail_lite::send -to_addr [ad_system_owner] -from_addr [ad_system_owner] -subject "Error reaching trados" -body "Trados Server not configured, please configure it first in the parameter section"
		return 0
	} else {
		set chilkat_ssh [intranet_chilkat::ssh_connect -server $trados_server -user $trados_user -port $trados_port -private_key $private_key]
		if {$chilkat_ssh eq 0} {
		    acs_mail_lite::send -to_addr [ad_system_owner] -from_addr [ad_system_owner] -subject "Error reaching trados" -body "Trados Server not reachable, please check your configuration"
			return 0
		}
	}
	
	set duration [time {intranet_chilkat::ssh_exec -command "$trados_console_app $config_file" -chilkat_ssh $chilkat_ssh}]
	
	# Disconnect SSH connection
	CkSsh_Disconnect $chilkat_ssh
	delete_CkSsh $chilkat_ssh

	return $duration
}


ad_proc -public im_trans_trados_create_analysis {
	-project_id
	{-target_language_id ""}
	-no_tasks:boolean
	{-filename ""}
} {
	Connects to a trados server and executes an analysis for each of the target languages

	@no_tasks Do not create the trados tasks, just store the analysis.
} {

	# ---------------------------------------------------------------
	# Get defaults
	# ---------------------------------------------------------------
	im_trans_trados_project_info -project_id $project_id -array_name project_info


	set project_dir $project_info(project_dir)
	set trados_dir $project_info(trados_dir)
	set trados_dir_win $project_info(trados_dir_win)

	if {"${project_dir}/Trados" ne $trados_dir} {
		im_filestorage_mkdir -dir $trados_dir
		im_filestorage_rsync -source_path ${project_dir}/Trados -target_path $trados_dir
	}

	db_1row project_info "select project_path,project_nr, company_id,source_language_id, subject_area_id, final_company_id from im_projects where project_id = :project_id"
	db_1row company "select company_path, company_name from im_companies where company_id = :company_id"

	set source_language [im_name_from_id $source_language_id]
	
	
	# Handle creation of the analysis for only a specific language
	if {$target_language_id ne ""} {
		set target_languages [im_name_from_id $target_language_id]
	} else {
		set target_languages [im_target_languages $project_id]
	}

	# ---------------------------------------------------------------
	# Prepare the config file
	# ---------------------------------------------------------------

	set trados_config "Mode#CreateAnalysis
ProjectFile#$trados_dir_win\\${project_nr}.sdlproj
StudioProjectFolder#$trados_dir_win
OrgFolder#$trados_dir_win\\Originals"

	set config_filename "$trados_dir/CreateAnalysis.config"
	set file [open "$config_filename" w+]
	fconfigure $file -encoding "utf-8"
	puts $file $trados_config
	flush $file
	close $file


	# ---------------------------------------------------------------
	# Run the analysis and copy it to the project dir
	# ---------------------------------------------------------------

	set duration [im_trans_trados_console_exec -config_file "${trados_dir_win}\\CreateAnalysis.config"]
	
#	file delete $config_filename
	
	if {"${project_dir}/Trados" ne $trados_dir} {
		im_filestorage_rsync -delete -source_path $trados_dir -target_path ${project_dir}/Trados
	}

	# ---------------------------------------------------------------
	# Load the results fo the analysis
	# ---------------------------------------------------------------

	set modification_date ""
	foreach target_language $target_languages {
		
		# Get the latest version of the wordcount analysis
		set wc_xml_file ""
		set modification_date ""
		
		if {[catch {glob -directory "${project_dir}/Trados/Reports" *${source_language}_${target_language}*} wc_files]} {
			set wc_files [list]
		}
		
		foreach wc_file $wc_files {
			set mod [file atime $wc_file]
			if {$mod>$modification_date} {
				set modification_date $mod
				set wc_xml_file $wc_file
		 	}
		}
		
		if {$wc_xml_file ne ""} {
			set target_language_id [cog_category_id -category $target_language -category_type "Intranet Translation Language"]
			im_trans_trados_create_tasks \
				-project_id $project_id \
				-trados_analysis_xml $wc_xml_file \
				-filename $filename \
				-target_language_id $target_language_id
			
		} else {
			ns_log Debug "Could not find WC XML File for $target_language in $project_nr"
		}
	}
}

ad_proc -public im_trans_trados_configured_p {
} {
	Return 1 if trados server is correctly configured and reachable
	
	Cached for 10 minutes
} {
	return [util_memoize  [list im_trans_trados_configured_helper] 600]
}

ad_proc -public im_trans_trados_configured_helper {
} {
	Return 1 if trados server is correctly configured and reachable
} {
	# Figure out if we can even run the analysis and set the parameters
	set trados_console_app [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosConsoleApp"]
	set trados_server [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosServer"]
	set trados_port [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosServerSSHPort"]
	set private_key [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosServerSSHKey"]
	set trados_user [parameter::get -package_id [im_package_trans_trados_id] -parameter "TradosUser"]

	set return_value 1
	
	if {$trados_console_app eq "" || $trados_server eq "" || $trados_port eq ""} {
		ns_log Notice "Trados Server not configured, please configure it first in the parameter section"
		set return_value 0
	} else {
		set chilkat_ssh [intranet_chilkat::ssh_connect -server $trados_server -user $trados_user -port $trados_port -private_key $private_key]
		if {$chilkat_ssh eq 0} {
			ns_log Notice "Trados Server not reachable, please check your configuration"
			set return_value 0
		} else {
			CkSsh_Disconnect $chilkat_ssh
			delete_CkSsh $chilkat_ssh
		}
	}

	return $return_value
}

ad_proc -public im_trans_trados_update_tm {
	-current_tm
	-new_tm
	-project_id
} {
	Update the current_tm with the new TUs from new_tm
} {
	im_trans_trados_project_info -project_id $project_id -array_name project_info
	
	set project_dir $project_info(project_dir)
	set trados_dir $project_info(trados_dir)
	set trados_dir_win $project_info(trados_dir_win)
	set project_nr $project_info(project_nr)
	set project_tmp_dir $project_info(project_tmp_dir)
    set project_tmp_dir_win $project_info(project_tmp_dir_win)
    set project_lead_id [db_string pl_lead "select project_lead_id from im_projects where project_id = :project_id" -default ""]
    if {$project_lead_id ne ""} {
	    set to_addr [party::email -party_id $project_lead_id]
	} else {
		set to_addr [ad_system_owner]
	}
	# Check the times first
	# If the project creation is newer than the modification
	# Time from the master TM, then we can just copy it over
	# And not bother with the merge
	
	set project_creation_date [db_string crea_date "select to_char(creation_date,'YYYY-MM-DD HH24:MI') from acs_objects where object_id = :project_id"]
	set current_mtime [file mtime $current_tm]
	set current_sql_time [clock format $current_mtime -format "%Y-%m-%d %T"]

	if {$current_sql_time > $project_creation_date} {
		
		# ---------------------------------------------------------------
		# Collect the files
		# ---------------------------------------------------------------
		
		im_filestorage_mkdir -dir ${project_tmp_dir}
		file copy -force ${project_dir}/Trados/${project_nr}.sdlproj $project_tmp_dir/${project_nr}.sdlproj
		file copy -force $current_tm ${project_tmp_dir}/current.sdltm
		file copy -force $new_tm ${project_tmp_dir}/new.sdltm
		
		set trados_config "Mode#UpdateTM
		CurrentTM#${project_tmp_dir_win}\\current.sdltm
		NewTM#${project_tmp_dir_win}\\new.sdltm
		StudioProjectFolder#${project_tmp_dir_win}
		ProjectFile#${project_tmp_dir_win}\\${project_nr}.sdlproj"
		
		set file [open "${project_tmp_dir}/UpdateTM.config" w+]
		fconfigure $file -encoding "utf-8"
		puts $file $trados_config
		flush $file
		close $file
		
		set duration [im_trans_trados_console_exec -config_file "${project_tmp_dir_win}\\UpdateTM.config"]
		
		ns_log Debug "Trados conversion for $current_tm and $new_tm in ${project_tmp_dir_win} took $duration"

		# Update the current file
		file delete -force $current_tm
		file copy -force "${project_tmp_dir}/current.sdltm" $current_tm

		set body "Trados conversion from<br/><i>$current_tm</i> to <br/><I>$new_tm</i> in<br><i> ${trados_dir}</i> took $duration"

		set file [open "${project_tmp_dir}/${project_nr}.log"]
		fconfigure $file -encoding "utf-8"
		set content [ad_convert_to_html [read $file]]
		set body "$body <hr /> $content"
		close $file

		# Delete the temporary files
		catch {file delete -force $project_tmp_dir}

		acs_mail_lite::send -send_immediately -to_addr $to_addr -mime_type "text/html" -from_addr $to_addr -subject "Master TM Merged $project_nr" -body	"$body"
		file mtime $current_tm [ns_time]
	} else {

	    # check if the file size is different. Only then overwrite
	    if {[file size $current_tm] ne [file size $new_tm]} {
		ns_log Debug "Overwriting TM $current_tm from $new_tm of project $project_id"

		acs_mail_lite::send -to_addr $to_addr -from_addr $to_addr -subject "Master TM Overwritten $project_nr" -body "Overwriting TM $current_tm from $new_tm of project $project_id"

		# Update the current file
		file delete -force $current_tm
		file copy -force $new_tm $current_tm
		file mtime $current_tm [ns_time]
	    }
	}


}


ad_proc -public im_trans_trados_create_package {
	-trans_task_ids
	{-task_type "trans"}
} {
	@param type Could be "Translate" or "Review"
} {
	
	# ---------------------------------------------------------------
	# Get defaults
	# ---------------------------------------------------------------
	set files [list]
	set comments [list]
	set sql "select project_id, target_language_id, description, source_language_id, task_filename, ${task_type}_id as freelancer_id from im_trans_tasks where task_id in ([ns_dbquotelist $trans_task_ids])"
	db_foreach task_infos $sql {
		lappend files $task_filename
		lappend comments $description
	}
	set files [join $files ","]
	
	im_trans_trados_project_info -project_id $project_id -array_name project_info
	
	switch $task_type {
		trans {
			set package_task "Translate"
		}
		edit {
			set package_task "Review"
		}
	}
	set project_dir $project_info(project_dir)
	set trados_dir $project_info(trados_dir)
	set trados_dir_win $project_info(trados_dir_win)
	
	if {"${project_dir}/Trados" ne $trados_dir} {
		im_filestorage_rsync -source_path ${project_dir}/Trados -target_path $trados_dir
	}
	
	db_1row project_info "select project_path,project_nr, company_id,source_language_id, subject_area_id, final_company_id from im_projects where project_id = :project_id"
	db_1row company "select company_path, company_name from im_companies where company_id = :company_id"

	set freelancer_name [im_name_from_id $freelancer_id]
	
	set source_language [im_name_from_id $source_language_id]
	
	if {$target_language_id eq ""} {
		set target_language [lindex [im_target_languages $project_id] 0]
	} else {
		set target_language [im_name_from_id $target_language_id]
	}
	

	# Calculate the due_days based on the task length
	set due_days [im_translation_task_processing_time -task_type $task_type -task_ids $trans_task_ids]
	
	set comment [join $comments "\n\r"]
	# ---------------------------------------------------------------
	# Prepare the config file
	# ---------------------------------------------------------------
	
	set trados_config "Mode#CreatePackage
ProjectFile#$trados_dir_win\\${project_nr}.sdlproj
StudioProjectFolder#$trados_dir_win
SourceLanguage#$source_language
TargetLanguage#$target_language
PackageTranslatorName#$freelancer_name
PackageTask#$package_task
DueInDays#$due_days
FilesInProject#$files"

# Comment#$comment"
	
	set config_filename "$trados_dir/CreatePackage_${freelancer_id}.config"
	set file [open "$config_filename" w+]
	fconfigure $file -encoding "utf-8"
	puts $file $trados_config
	flush $file
	close $file
	
	
	# ---------------------------------------------------------------
	# Run the package creation
	# ---------------------------------------------------------------
	
	ns_log Debug "Starting the package creation of $project_nr"
	
	set duration [im_trans_trados_console_exec -config_file "${trados_dir_win}\\CreatePackage_${freelancer_id}.config"]
	
	ns_log Debug "Package Creation for freelancer $freelancer_name in $project_nr took $duration"
	
	
	if {"${project_dir}/Trados" ne $trados_dir} {
		im_filestorage_rsync -delete -source_path $trados_dir -target_path ${project_dir}/Trados
	}
	
	return "${trados_dir}/${project_nr}_${source_language}_${target_language}_${freelancer_name}.sdlppx"
}
