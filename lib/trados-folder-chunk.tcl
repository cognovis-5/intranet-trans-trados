ad_page_contract {
	page to view translation memory

	@author malte.sussdorff@cognovis.de
	@creation-date 2015-11-12
	@cvs-id $Id$
} -query {
	{orderby:optional}
} -properties {
	company_id:onevalue
	return_url:onevalue
}

set viewing_user_id [ad_conn user_id]
set folder_id [im_trans_trados_folder_id -company_id $company_id]
if {$folder_id eq ""} {
	return ""
	ad_script_abort
}
# permission::require_permission -party_id $viewing_user_id -object_id $folder_id -privilege "read"

set admin_p [permission::permission_p -party_id $viewing_user_id -object_id $folder_id -privilege "admin"]

set write_p $admin_p

if {!$write_p} {
    set write_p [permission::permission_p -party_id $viewing_user_id -object_id $folder_id -privilege "write"]
}
if {![exists_and_not_null n_past_days]} {
    set n_past_days 99999
}

if {![exists_and_not_null fs_url]} {
    set fs_url "/file-storage/"
}

set folder_name [lang::util::localize [fs::get_object_name -object_id  $folder_id]]

set content_size_total 0

if {![exists_and_not_null format]} {
    set format table
}

#AG: We're an include file, and we may be included from outside file-storage.
#So we need to query for the package_id rather than getting it from ad_conn.
set package_and_root [fs::get_folder_package_and_root $folder_id]
set package_id [lindex $package_and_root 0]
if {![exists_and_not_null root_folder_id]} {
    set root_folder_id [lindex $package_and_root 1]
}

if { $root_folder_id ne $folder_id } {
    set folder_path "[db_exec_plsql get_folder_path {select content_item__get_path(:folder_id, :root_folder_id)}]/"
} else {
    set folder_path ""
}

set actions [list]

# for now, invite users to upload, and then they will be asked to
# login if they are not.

set cancel_url "[ad_conn url]?[ad_conn query]"

lappend actions "#file-storage.Add_File#" [export_vars -base "/intranet-trans-trados/file-add" {company_id cancel_url {return_url $cancel_url}}] "[_ file-storage.lt_Upload_a_file_in_this]" 

set expose_rss_p [parameter::get -parameter ExposeRssP -package_id $package_id -default 0]
set like_filesystem_p [parameter::get -parameter BehaveLikeFilesystemP -package_id $package_id -default 1]

set target_window_name [parameter::get -parameter DownloadTargetWindowName -package_id $package_id -default ""]
if { [string equal $target_window_name ""] } {
    set target_attr ""
} else {
    set target_attr "target=\"$target_window_name\""
}

if {$admin_p} {
    lappend actions "#file-storage.lt_Modify_permissions_on_1#" [export_vars -base "${fs_url}permissions" -override {{object_id $folder_id}} {{return_url "[util_get_current_url]"}}] "#file-storage.lt_Modify_permissions_on_1#"
    if { $expose_rss_p } {
        lappend actions "Configure RSS" [export_vars -base "${fs_url}admin/rss-subscrs" {folder_id}] "Configure RSS"
    }
}
set categories_p [parameter::get -parameter CategoriesP -package_id $package_id -default 0]
if { $categories_p } {
    if { [permission::permission_p -party_id $viewing_user_id -object_id $package_id -privilege "admin"] } {
        lappend actions [_ categories.cadmin] [export_vars -base "/categories/cadmin/object-map" -url {{object_id $package_id}}] [_ categories.cadmin]
    }
    set category_links [fs::category_links -object_id $folder_id -folder_id $folder_id -selected_category_id $category_id -fs_url $fs_url]
}

#set n_past_filter_values [list [list "Yesterday" 1] [list [_ file-storage.last_week] 7] [list [_ file-storage.last_month] 30]]
set elements [list \
                  name \
                  [list label [_ file-storage.Name] \
                       display_template {<a @target_attr@ href="@contents.file_url@" title="\#file-storage.view_contents\#"><if @contents.title@ nil>@contents.name@</a></if><else>@contents.title@</a><br><if @contents.name@ ne @contents.title@>@contents.name@</if></else>} \
                       orderby_desc {fs_objects.name desc} \
                       orderby_asc {fs_objects.name asc}] \
                  content_size_pretty \
                  [list label [_ file-storage.Size] \
                       display_template {@contents.content_size_pretty;noquote@} \
                       orderby_desc {content_size desc} \
                       orderby_asc {content_size asc}] \
                  last_modified_pretty \
                  [list label [_ file-storage.Last_Modified] \
                       orderby_desc {last_modified_ansi desc} \
                       orderby_asc {last_modified_ansi asc}] \
		 properties_link \
		  [list label "[_ file-storage.properties]" \
		       link_url_col properties_url \
		       link_html { title "[_ file-storage.properties]" }] \
	     ] 



if { $categories_p } {
    lappend elements categories [list label [_ file-storage.Categories] display_col "categories;noquote"]
}

lappend elements views [list label "Views" ]



if {[apm_package_installed_p views]} {
    concat $elements [list views [list label "Views"]]
}

if {[exists_and_not_null return_url]} {
    set return_url [export_vars -base "index" {folder_id}]
}
set vars_to_export [list return_url]

set bulk_actions [list "[_ file-storage.Delete]" "${fs_url}delete" "[_ file-storage.Delete_Checked_Items]" "[_ file-storage.Download_ZIP]" "${fs_url}download-zip" "[_ file-storage.Download_ZIP_Checked_Items]"]
callback fs::folder_chunk::add_bulk_actions \
        -bulk_variable "bulk_actions" \
        -folder_id $folder_id \
        -var_export_list "vars_to_export"

if {$format eq "list"} { 
    set actions {}
} 

template::list::create \
    -name contents_${folder_id} \
    -multirow contents \
    -key object_id \
    -actions $actions \
    -bulk_actions $bulk_actions \
    -bulk_action_export_vars $vars_to_export \
    -selected_format $format \
    -formats {
        table {
            label Table
            layout table
        }
        list {
            label List
            layout list
            template {
                <listelement name="short_name"> - <listelement name="last_modified_pretty">  
            }
        }
    } \
    -pass_properties [list target_attr] \
    -filters {
		project_id {}
		company_id {}
        folder_id {hide_p 1}
        page_num
    } \
    -elements $elements

set orderby [template::list::orderby_clause -orderby -name contents_${folder_id}]

if {[string equal $orderby ""]} {
    set orderby " order by fs_objects.sort_key, fs_objects.name asc"
}

if { $categories_p && [exists_and_not_null category_id] } {
    set categories_limitation [db_map categories_limitation]
} else {
    set categories_limitation {}
}


db_multirow -extend {label alt_icon icon last_modified_pretty content_size_pretty properties_link properties_url download_link download_url new_version_link new_version_url views categories approve_url disapprove_url publish_url unpublish_url file_state_pretty} contents select_folder_contents "
	select cr.publish_status as file_state,
				   cr.item_id as item_id,
				   fs_objects.object_id,
					   fs_objects.mime_type,
				   fs_objects.name,
					   fs_objects.live_revision,
					   to_char(fs_objects.last_modified, 'YYYY-MM-DD HH24:MI:SS') as last_modified_ansi,
					   fs_objects.content_size,
					   fs_objects.url,
					   fs_objects.sort_key,
					   -fs_objects.sort_key as sort_key_desc,
					   fs_objects.file_upload_name,
					   fs_objects.title,
					   case
						 when :folder_path is null
						 then fs_objects.file_upload_name
						 else :folder_path || fs_objects.file_upload_name
					   end as file_url,
					   case
						 when fs_objects.last_modified >= (now() - cast('$n_past_days days' as interval))
						 then 1
						 else 0
					   end as new_p
				from fs_objects, cr_items cr
				where fs_objects.parent_id = :folder_id
			and cr.item_id = fs_objects.object_id
				and exists (select 1
					   from acs_object_party_privilege_map m
					   where m.object_id = fs_objects.object_id
						 and m.party_id = :viewing_user_id
						 and m.privilege = 'read')
					$categories_limitation
			$orderby
" {
    
    set file_state_pretty $file_state
    set change_item_url "/file-storage/change-file-state"
    set return_url [util_get_current_url]

    switch $file_state {
	ready { 
	    set disapprove_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status production} {return_url}}]
	    set publish_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status live} {return_url}}]
	    set unpublish_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status expired} {return_url}}]
	}
	production {
	    set approve_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status ready} {return_url}}]
	    set publish_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status live} {return_url}}]
	    set unpublish_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status expired} {return_url}}]
	}
	expired {
	    set disapprove_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status production} {return_url}}]
	    set approve_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status ready} {return_url}}]
	    set publish_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status live} {return_url}}]
	}
	live {
	    set disapprove_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status production} {return_url}}]
	    set approve_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status ready} {return_url}}]
	    set unpublish_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status expired} {return_url}}]
	    
	}
    }
    #if user is not Ben Bigboss revoke all actions
    #Error SWA has no actions buttons
    set po_admin_p [group::party_member_p -party_id [ad_conn user_id -group_name "P/O Admin"]]
    set admin_p [acs_user::site_wide_admin_p -user_id [ad_conn user_id]]
    if {!$admin_p && !$po_admin_p} {
	set approve_url ""
	set disapprove_url ""
	set publish_url ""
	set unpublish_url ""
    }

    #if user is project manager and not po admin grant publish privilege
    set p_manager_p [group::party_member_p -party_id [ad_conn user_id] -group_name "Project Managers"]
    if {$p_manager_p && !$po_admin_p} {
		set publish_url [export_vars -base "$change_item_url" {{item_id} {live_revision} {status live} {return_url}}]
    }

    set last_modified_ansi [lc_time_system_to_conn $last_modified_ansi]
    
    set last_modified_pretty [lc_time_fmt $last_modified_ansi "%x %X"]
    if { $content_size eq "" } {
        set content_size_pretty ""
    } elseif {$content_size < 1024} {
        set content_size_pretty "[lc_numeric $content_size]&nbsp;[_ file-storage.bytes]"
    } else {
        set content_size_pretty "[lc_numeric [expr $content_size / 1024 ]]&nbsp;[_ file-storage.kb]"
    }

    set file_upload_name [fs::remove_special_file_system_characters -string $file_upload_name]

    if { ![empty_string_p $content_size] } {
        incr content_size_total $content_size
    }

    set views ""
    if {[apm_package_installed_p views]} {
        array set views_arr [views::get -object_id $object_id] 
        if {$views_arr(views_count) ne ""} {
            set views " $views_arr(views_count) / $views_arr(unique_views)"
        }
    }

    set name [lang::util::localize $name]
    set owner_p [acs_object::get_element -object_id $item_id -element creation_user]
    if {$owner_p eq [ad_conn user_id] || $admin_p} {
		set properties_link [_ file-storage.properties]
		set properties_url [export_vars -base "${fs_url}file" {{file_id $object_id} return_url}]
		set new_version_link [_ acs-kernel.common_New]
		set new_version_url [export_vars -base "${fs_url}file-add" {{file_id $object_id} return_url}]
    } else { 
		set properties_link ""
		set properties_url ""
		set new_version_link ""
		set new_version_url ""
    }
    set icon "/resources/file-storage/file.gif"
    set alt_icon "#file-storage.file#"
    set file_url "${fs_url}view/${folder_path}[ad_urlencode ${name}]"
    set download_link [_ file-storage.Download]
    set file_url [export_vars -base "${fs_url}download/[ad_urlencode $title]" {{file_id $object_id}}]

}

if {$format eq "list"} {
    set content_size_total 0
}
ad_return_template
