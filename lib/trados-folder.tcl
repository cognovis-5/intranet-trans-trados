ad_page_contract {
	page to view translation memory

	@author malte.sussdorff@cognovis.de
	@creation-date 2015-11-12
	@cvs-id $Id$
} -query {
	{orderby:optional}
} -properties {
	company_id:onevalue
	project_id:onevalue
	return_url:onevalue
}

if {[info exists project_id]} {
	set trados_project_p [im_trans_trados_project_p -project_id $project_id]
} else {
	set trados_project_p 1
}

# ---------------------------------------------------------------
# prepare list of files
# ---------------------------------------------------------------

set actions [list]


# for now, invite users to upload, and then they will be asked to
# login if they are not.

set cancel_url "[ad_conn url]?[ad_conn query]"

set elements {
	file_name_pretty {
		label {[_ file-storage.Name]} \
		display_template {
		    @contents.file_name_pretty;noquote@
		}
	}
	source_language {
		label {[_ intranet-translation.Source_Language]}
	}
	target_language {
		label {[_ intranet-translation.Target_Language]}
	}
	subject_area {
		label {[_ intranet-translation.Subject_Area]}
	}
	file_size {
		label {[_ intranet-core.Size]}
	}
	file_modified {
		label {[_ intranet-translation.Modified_Date]}
		display_template {
			@contents.file_modified;noquote@
		}
	}
	file_type {
		label {[_ intranet-core.Type]}
	}
}

template::multirow create contents file_name file_name_pretty source_language target_language subject_area file_size file_modified file_type


# ---------------------------------------------------------------
# Find the files and append to multirow
# ---------------------------------------------------------------

if {[exists_and_not_null company_id]} {
    # Get all TM & TB for the company
    
    set find_cmd [im_filestorage_find_cmd]
    set find_path [im_trans_trados_folder -company_id $company_id]
    if {$find_path ne ""} {
	set file_list [exec $find_cmd $find_path -noleaf]
	set files [lsort [split $file_list "\n"]]
	# remove the first (root path) from the list of files returned by "find".
	set files [lrange $files 1 [llength $files]]
	
	lappend actions "#file-storage.Add_File#" [export_vars -base "/intranet-trans-trados/file-add" {company_id cancel_url {return_url $cancel_url}}] "[_ file-storage.lt_Upload_a_file_in_this]" 		 
    } else {
	set files ""
    }

} elseif {[exists_and_not_null project_id]} {
	
	im_trans_trados_project_info -project_id $project_id -array_name project_info
	
	set project_dir $project_info(project_dir)
	set trados_dir $project_info(trados_dir)

	# Get all the TM to be used in the project
	# This should be in line with trados analysis logic
	db_1row project_info "select company_id, final_company_id, source_language_id,subject_area_id,to_char(start_date,'YYYY-MM-DD') as start_date from im_projects where project_id = :project_id"
	set target_language_ids [db_list target_langs "select language_id from im_target_languages where project_id = :project_id"]
	
	set files [list]
		
	foreach target_language_id $target_language_ids {
				
		# Find the correct translation memories
		foreach file_path [im_trans_trados_get_trans_memory_file \
			-company_id $company_id \
			-final_company_id $final_company_id \
			-source_language_id $source_language_id \
			-target_language_id $target_language_id \
			-subject_area_id $subject_area_id] {
				lappend files $file_path
			}
	}
	
} else {
	set files [list]
}

set file_modified_p 0

foreach file $files {
    set file_paths [split $file "/"]
    set file_paths_len [llength $file_paths]
    
    # Get the last part of the path to retrieve the filename
    set file_name [lindex $file_paths [expr $file_paths_len -1]]

    # Quick hack to get around the __ problem when spitting
    regsub -all {__} $file_name {?} file_name_sub
    set file_name_parts [split [file rootname $file_name_sub] {?}]
    
    if {$company_id eq ""} {
	set company_path [lindex $file_name_parts 1]
	set company_id [db_string company_id "select company_id from im_companies where company_path = :company_path" -default ""]
    }
    
    set file_size "[expr [file size $file] / 1024] KB"
    set file_modified [ns_fmttime [file mtime $file] "%d/%m/%Y %H:%M"]
    set file_extension [file extension $file]
    set file_name_pretty $file_name
    if {[exists_and_not_null project_id]} {
	set orig_file_mtime [file mtime $file]
	
	switch $file_extension {
	    .sdltm {
		set project_file_path "${trados_dir}/TM/$file_name"
	    }
	    .sdltb {
		set project_file_path "${trados_dir}/TB/$file_name"
	    }
	    default {
		set project_file_path ""
	    }
	}
	if {[file exists $project_file_path]} {
	    set file_name_pretty "<a href='[export_vars -base "/intranet-trans-trados/download" -url {{file_path $project_file_path}}]'>$file_name</a>"
	    set project_file_mtime [file mtime $project_file_path]
	    set file_modified [ns_fmttime [file mtime $project_file_path] "%d/%m/%Y %H:%M"]
	    
	    if {$project_file_mtime < $orig_file_mtime} {
		# Make sure to highlight that the TM/TB has changed after project was created
		set file_modified "<font color=red>$file_modified</font>"
		set file_name "<font color=red>$file_name</font>"
		set file_modified_p 1
	    }
	    
	} else {
	    set file_name_pretty "<a href='[export_vars -base "/intranet-trans-trados/download" -url {{file_path $file}}]'>$file_name</a>"
	}
    } else {
	set file_name_pretty "<a href='[export_vars -base "/intranet-trans-trados/download" -url {{file_path $file}}]'>$file_name</a>"
    }

    # Make the filename downloadable
    switch $file_extension {
	.sdltm {
	    set source_language [lindex $file_name_parts 2]
	    set target_language [lindex $file_name_parts 3]
	    set subject_area [lindex $file_name_parts 4]
	    set file_type "Translation Memory"
	    if {$company_id ne "" && $file_name ne ""} {
		template::multirow append contents $file_name $file_name_pretty $source_language $target_language $subject_area $file_size $file_modified $file_type
	    } else {
		ds_comment "path:: $file_name_parts"
	    }
	}
	.sdltb {
	    set source_language ""
	    set target_language ""
	    set subject_area ""
	    set file_type "Termbase"
	    if {$company_id ne "" && $file_name ne ""} {
		template::multirow append contents $file_name $file_name_pretty $source_language $target_language $subject_area $file_size $file_modified $file_type
	    } else {
		ds_comment "path:: $file_name_parts"
	    }
	}
	default {
	}
    }	
}

if {[exists_and_not_null project_id]} {
	# Check if we have packages created. If yes, hide the link
	set project_dir [cog::project::path -project_id $project_id]
	if {![file exists "${project_dir}/Trados/Packages/Out"] && $file_modified_p} {
		lappend actions "#intranet-trans-trados.Update_TMB#" [export_vars -base "/intranet-trans-trados/update-tmbs-from-company" {project_id}] ""
	}
	
	
	# ---------------------------------------------------------------
	# Updating of TMs in between
	set closed_ids [im_sub_categories 81]
	set status_id [db_string status "select project_status_id from im_projects where project_id = :project_id" -default ""]
#	if {[lsearch $closed_ids $status_id]<0} {
		lappend actions "#intranet-trans-trados.Update_TMB_from_folder#"	[export_vars -base "/intranet-trans-trados/update-tmbs-from-folder" -url {project_id}] 
#	}

}

template::list::create \
	-name contents \
	-multirow contents \
	-key file_name \
	-actions $actions \
	-elements $elements \
	-bulk_actions [list [_ intranet-core.Delete] "/intranet-trans-trados/delete-tm" "Delete selected TM"] \
	-bulk_action_export_vars { company_id return_url } \
	-bulk_action_method post


ad_return_template
