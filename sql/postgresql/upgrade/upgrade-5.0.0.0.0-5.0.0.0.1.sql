SELECT acs_log__debug('/packages/intranet-trans-trados/sql/postgresql/upgrade/upgrade-5.0.0.0.0-5.0.0.0.1.sql','');
update apm_parameter_values set attr_value = '/var/www/openacs/filestorage/trados' where parameter_id in (select parameter_id from apm_parameters where parameter_name = 'TradosCompanyUnixPath' and package_key = 'intranet-trans-trados');
update apm_parameter_values set attr_value = '/var/www/openacs/filestorage/projects' where parameter_id in (select parameter_id from apm_parameters where parameter_name = 'TradosUnixPath' and package_key = 'intranet-trans-trados');
